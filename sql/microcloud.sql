/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50725
Source Host           : 127.0.0.1:3306
Source Database       : microcloud

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2020-10-04 17:33:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acl_permission
-- ----------------------------
DROP TABLE IF EXISTS `acl_permission`;
CREATE TABLE `acl_permission` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '编号',
  `pid` char(19) NOT NULL DEFAULT '' COMMENT '所属上级',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '类型(1:菜单,2:按钮)',
  `permission_value` varchar(50) DEFAULT NULL COMMENT '权限值',
  `path` varchar(100) DEFAULT NULL COMMENT '访问路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件路径',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态(0:禁止,1:正常)',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限';

-- ----------------------------
-- Records of acl_permission
-- ----------------------------
INSERT INTO `acl_permission` VALUES ('1', '0', '全部数据', '0', null, null, null, null, null, '0', '2019-11-15 17:13:06', '2019-11-15 17:13:06');
INSERT INTO `acl_permission` VALUES ('1195268474480156673', '1', '权限管理', '1', null, '/acl', 'Layout', null, null, '0', '2019-11-15 17:13:06', '2019-11-18 13:54:25');
INSERT INTO `acl_permission` VALUES ('1195268616021139457', '1195268474480156673', '用户管理', '1', null, 'user/list', '/acl/user/list', null, null, '0', '2019-11-15 17:13:40', '2019-11-18 13:53:12');
INSERT INTO `acl_permission` VALUES ('1195268788138598401', '1195268474480156673', '角色管理', '1', null, 'role/list', '/acl/role/list', null, null, '0', '2019-11-15 17:14:21', '2019-11-15 17:14:21');
INSERT INTO `acl_permission` VALUES ('1195268893830864898', '1195268474480156673', '菜单管理', '1', null, 'menu/list', '/acl/menu/list', null, null, '0', '2019-11-15 17:14:46', '2019-11-15 17:14:46');
INSERT INTO `acl_permission` VALUES ('1195269143060602882', '1195268616021139457', '查看', '2', 'user.list', '', '', null, null, '0', '2019-11-15 17:15:45', '2019-11-17 21:57:16');
INSERT INTO `acl_permission` VALUES ('1195269295926206466', '1195268616021139457', '添加', '2', 'user.add', 'user/add', '/acl/user/form', null, null, '0', '2019-11-15 17:16:22', '2019-11-15 17:16:22');
INSERT INTO `acl_permission` VALUES ('1195269473479483394', '1195268616021139457', '修改', '2', 'user.update', 'user/update/:id', '/acl/user/form', null, null, '0', '2019-11-15 17:17:04', '2019-11-15 17:17:04');
INSERT INTO `acl_permission` VALUES ('1195269547269873666', '1195268616021139457', '删除', '2', 'user.remove', '', '', null, null, '0', '2019-11-15 17:17:22', '2019-11-15 17:17:22');
INSERT INTO `acl_permission` VALUES ('1195269821262782465', '1195268788138598401', '修改', '2', 'role.update', 'role/update/:id', '/acl/role/form', null, null, '0', '2019-11-15 17:18:27', '2019-11-15 17:19:53');
INSERT INTO `acl_permission` VALUES ('1195269903542444034', '1195268788138598401', '查看', '2', 'role.list', '', '', null, null, '0', '2019-11-15 17:18:47', '2019-11-15 17:18:47');
INSERT INTO `acl_permission` VALUES ('1195270037005197313', '1195268788138598401', '添加', '2', 'role.add', 'role/add', '/acl/role/form', null, null, '0', '2019-11-15 17:19:19', '2019-11-18 11:05:42');
INSERT INTO `acl_permission` VALUES ('1195270442602782721', '1195268788138598401', '删除', '2', 'role.remove', '', '', null, null, '0', '2019-11-15 17:20:55', '2019-11-15 17:20:55');
INSERT INTO `acl_permission` VALUES ('1195270621548568578', '1195268788138598401', '角色权限', '2', 'role.acl', 'role/distribution/:id', '/acl/role/roleForm', null, null, '0', '2019-11-15 17:21:38', '2019-11-15 17:21:38');
INSERT INTO `acl_permission` VALUES ('1195270744097742849', '1195268893830864898', '查看', '2', 'permission.list', '', '', null, null, '0', '2019-11-15 17:22:07', '2019-11-15 17:22:07');
INSERT INTO `acl_permission` VALUES ('1195270810560684034', '1195268893830864898', '添加', '2', 'permission.add', '', '', null, null, '0', '2019-11-15 17:22:23', '2019-11-15 17:22:23');
INSERT INTO `acl_permission` VALUES ('1195270862100291586', '1195268893830864898', '修改', '2', 'permission.update', '', '', null, null, '0', '2019-11-15 17:22:35', '2019-11-15 17:22:35');
INSERT INTO `acl_permission` VALUES ('1195270887933009922', '1195268893830864898', '删除', '2', 'permission.remove', '', '', null, null, '0', '2019-11-15 17:22:41', '2019-11-15 17:22:41');
INSERT INTO `acl_permission` VALUES ('1195349439240048642', '1', '讲师管理', '1', null, '/edu/teacher', 'Layout', null, null, '0', '2019-11-15 22:34:49', '2019-11-15 22:34:49');
INSERT INTO `acl_permission` VALUES ('1195349699995734017', '1195349439240048642', '讲师列表', '1', null, 'list', '/edu/teacher/list', null, null, '0', '2019-11-15 22:35:52', '2019-11-15 22:35:52');
INSERT INTO `acl_permission` VALUES ('1195349810561781761', '1195349439240048642', '添加讲师', '1', null, 'save', '/edu/teacher/save', null, null, '0', '2019-11-15 22:36:18', '2019-11-15 22:36:18');
INSERT INTO `acl_permission` VALUES ('1195349876252971010', '1195349810561781761', '添加', '2', 'teacher.add', '', '', null, null, '0', '2019-11-15 22:36:34', '2019-11-15 22:36:34');
INSERT INTO `acl_permission` VALUES ('1195350117270261762', '1195349699995734017', '修改', '2', 'teacher.update', 'edit/:id', '/edu/teacher/save', null, null, '0', '2019-11-15 22:37:31', '2019-11-15 22:37:31');
INSERT INTO `acl_permission` VALUES ('1195350188359520258', '1195349699995734017', '删除', '2', 'teacher.remove', '', '', null, null, '0', '2019-11-15 22:37:48', '2019-11-15 22:37:48');
INSERT INTO `acl_permission` VALUES ('1195350299365969922', '1', '课程分类', '1', null, '/edu/subject', 'Layout', null, null, '0', '2019-11-15 22:38:15', '2019-11-15 22:38:15');
INSERT INTO `acl_permission` VALUES ('1195350397751758850', '1195350299365969922', '课程分类列表', '1', null, 'list', '/edu/subject/list', null, null, '0', '2019-11-15 22:38:38', '2019-11-15 22:38:38');
INSERT INTO `acl_permission` VALUES ('1195350500512206850', '1195350299365969922', '导入课程分类', '1', null, 'save', '/edu/subject/save', null, null, '0', '2019-11-15 22:39:03', '2019-11-15 22:39:03');
INSERT INTO `acl_permission` VALUES ('1195350687590748161', '1195350500512206850', '导入', '2', 'subject.import', '', '', null, null, '0', '2019-11-15 22:39:47', '2019-11-15 22:39:47');
INSERT INTO `acl_permission` VALUES ('1195350831744782337', '1', '课程管理', '1', null, '/edu/course', 'Layout', null, null, '0', '2019-11-15 22:40:21', '2019-11-15 22:40:21');
INSERT INTO `acl_permission` VALUES ('1195350919074385921', '1195350831744782337', '课程列表', '1', null, 'list', '/edu/course/list', null, null, '0', '2019-11-15 22:40:42', '2019-11-15 22:40:42');
INSERT INTO `acl_permission` VALUES ('1195351020463296513', '1195350831744782337', '发布课程', '1', null, 'info', '/edu/course/info', null, null, '0', '2019-11-15 22:41:06', '2019-11-15 22:41:06');
INSERT INTO `acl_permission` VALUES ('1195351159672246274', '1195350919074385921', '删除课程', '2', 'course.remove', '', '', null, null, '0', '2019-11-15 22:41:40', '2020-09-24 17:46:06');
INSERT INTO `acl_permission` VALUES ('1195351326706208770', '1195350919074385921', '编辑课程', '2', 'course.update', 'info/:id', '/edu/course/info', null, null, '0', '2019-11-15 22:42:19', '2019-11-15 22:42:19');
INSERT INTO `acl_permission` VALUES ('1195351566221938690', '1195350919074385921', '编辑课程大纲', '2', 'chapter.update', 'chapter/:id', '/edu/course/chapter', null, null, '0', '2019-11-15 22:43:17', '2019-11-15 22:43:17');
INSERT INTO `acl_permission` VALUES ('1195351862889254913', '1', '统计分析', '1', null, '/sta', 'Layout', null, null, '0', '2019-11-15 22:44:27', '2019-11-15 22:44:27');
INSERT INTO `acl_permission` VALUES ('1195351968841568257', '1195351862889254913', '生成统计', '1', null, 'create', '/sta/create', null, null, '0', '2019-11-15 22:44:53', '2019-11-15 22:44:53');
INSERT INTO `acl_permission` VALUES ('1195352054917074946', '1195351862889254913', '统计图表', '1', null, 'show', '/sta/show', null, null, '0', '2019-11-15 22:45:13', '2019-11-15 22:45:13');
INSERT INTO `acl_permission` VALUES ('1195352127734386690', '1195352054917074946', '查看', '2', 'daily.list', '', '', null, null, '0', '2019-11-15 22:45:30', '2019-11-15 22:45:30');
INSERT INTO `acl_permission` VALUES ('1195352215768633346', '1195351968841568257', '生成', '2', 'daily.add', '', '', null, null, '0', '2019-11-15 22:45:51', '2019-11-15 22:45:51');
INSERT INTO `acl_permission` VALUES ('1196301740985311234', '1195268616021139457', '分配角色', '2', 'user.assgin', 'user/role/:id', '/acl/user/roleForm', null, null, '0', '2019-11-18 13:38:56', '2019-11-18 13:38:56');
INSERT INTO `acl_permission` VALUES ('1309067051809189889', '1195351020463296513', '发布', '2', 'course.publish', 'publish/:id', '/edu/course/publish', null, null, '0', '2020-09-24 17:48:01', '2020-09-30 22:19:55');
INSERT INTO `acl_permission` VALUES ('1309071540834349058', '1195350397751758850', '查看', '2', 'subject.list', '', '', null, null, '0', '2020-09-24 18:05:51', '2020-09-24 18:05:51');
INSERT INTO `acl_permission` VALUES ('1309071826932019201', '1195350919074385921', '查看', '2', 'course.list', '', '', null, null, '0', '2020-09-24 18:07:00', '2020-09-24 18:07:11');
INSERT INTO `acl_permission` VALUES ('1309071994163113985', '1195349699995734017', '查看', '2', 'teacher.list', '', '', null, null, '0', '2020-09-24 18:07:39', '2020-09-24 18:07:39');

-- ----------------------------
-- Table structure for acl_role
-- ----------------------------
DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE `acl_role` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '角色id',
  `role_name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_code` varchar(20) DEFAULT NULL COMMENT '角色编码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_role
-- ----------------------------
INSERT INTO `acl_role` VALUES ('1', '超级管理员', null, null, '0', '2019-11-11 13:09:32', '2020-09-24 18:00:42');
INSERT INTO `acl_role` VALUES ('1193757683205607426', '课程管理员', null, null, '0', '2019-11-11 13:09:45', '2020-09-24 16:56:12');
INSERT INTO `acl_role` VALUES ('1309070391716687873', '普通管理员', null, null, '0', '2020-09-24 18:01:17', '2020-09-24 18:01:17');
INSERT INTO `acl_role` VALUES ('1309070648718471170', '数据分析员', null, null, '0', '2020-09-24 18:02:19', '2020-09-24 18:02:19');

-- ----------------------------
-- Table structure for acl_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `acl_role_permission`;
CREATE TABLE `acl_role_permission` (
  `id` char(19) NOT NULL DEFAULT '',
  `role_id` char(19) NOT NULL DEFAULT '',
  `permission_id` char(19) NOT NULL DEFAULT '',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限';

-- ----------------------------
-- Records of acl_role_permission
-- ----------------------------
INSERT INTO `acl_role_permission` VALUES ('1309070283558170626', '1', '1', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364930', '1', '1195268474480156673', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364931', '1', '1195268616021139457', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364932', '1', '1195269143060602882', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364933', '1', '1195269295926206466', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364934', '1', '1195269473479483394', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283562364935', '1', '1195269547269873666', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559233', '1', '1196301740985311234', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559234', '1', '1195268788138598401', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559235', '1', '1195269821262782465', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559236', '1', '1195269903542444034', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559237', '1', '1195270037005197313', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283566559238', '1', '1195270442602782721', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283570753537', '1', '1195270621548568578', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283570753538', '1', '1195268893830864898', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947842', '1', '1195270744097742849', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947843', '1', '1195270810560684034', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947844', '1', '1195270862100291586', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947845', '1', '1195270887933009922', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947846', '1', '1195349439240048642', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283574947847', '1', '1195349699995734017', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142145', '1', '1195350117270261762', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142146', '1', '1195350188359520258', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142147', '1', '1195349810561781761', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142148', '1', '1195349876252971010', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142149', '1', '1195350299365969922', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142150', '1', '1195350397751758850', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142151', '1', '1195350500512206850', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283579142152', '1', '1195350687590748161', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283583336450', '1', '1195350831744782337', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283583336451', '1', '1195350919074385921', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852290', '1', '1195351159672246274', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852291', '1', '1195351326706208770', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852292', '1', '1195351566221938690', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852293', '1', '1195351020463296513', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852294', '1', '1309067051809189889', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852295', '1', '1195351862889254913', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852296', '1', '1195351968841568257', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852297', '1', '1195352215768633346', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852298', '1', '1195352054917074946', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070283939852299', '1', '1195352127734386690', '0', '2020-09-24 18:00:52', '2020-09-24 18:00:52');
INSERT INTO `acl_role_permission` VALUES ('1309070611217199105', '1309070391716687873', '1', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611217199106', '1309070391716687873', '1195268474480156673', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611221393409', '1309070391716687873', '1195268616021139457', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611221393410', '1309070391716687873', '1195269143060602882', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611221393411', '1309070391716687873', '1195269295926206466', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611225587713', '1309070391716687873', '1195269473479483394', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611225587714', '1309070391716687873', '1195269547269873666', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611225587715', '1309070391716687873', '1196301740985311234', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782018', '1309070391716687873', '1195268788138598401', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782019', '1309070391716687873', '1195269821262782465', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782020', '1309070391716687873', '1195269903542444034', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782021', '1309070391716687873', '1195270037005197313', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782022', '1309070391716687873', '1195270442602782721', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782023', '1309070391716687873', '1195270621548568578', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782024', '1309070391716687873', '1195268893830864898', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782025', '1309070391716687873', '1195270744097742849', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782026', '1309070391716687873', '1195270810560684034', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782027', '1309070391716687873', '1195270862100291586', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782028', '1309070391716687873', '1195270887933009922', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782029', '1309070391716687873', '1195349439240048642', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782030', '1309070391716687873', '1195349699995734017', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782031', '1309070391716687873', '1195350117270261762', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782032', '1309070391716687873', '1195350188359520258', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782033', '1309070391716687873', '1195349810561781761', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070611229782034', '1309070391716687873', '1195349876252971010', '0', '2020-09-24 18:02:10', '2020-09-24 18:02:10');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603201', '1309070648718471170', '1', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603202', '1309070648718471170', '1195351862889254913', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603203', '1309070648718471170', '1195351968841568257', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603204', '1309070648718471170', '1195352215768633346', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603205', '1309070648718471170', '1195352054917074946', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309070682935603206', '1309070648718471170', '1195352127734386690', '0', '2020-09-24 18:02:27', '2020-09-24 18:02:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198486204417', '1193757683205607426', '1', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398721', '1193757683205607426', '1195350299365969922', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398722', '1193757683205607426', '1195350397751758850', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398723', '1193757683205607426', '1309071540834349058', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398724', '1193757683205607426', '1195350500512206850', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398725', '1193757683205607426', '1195350687590748161', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398726', '1193757683205607426', '1195350831744782337', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398727', '1193757683205607426', '1195350919074385921', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398728', '1193757683205607426', '1195351159672246274', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398729', '1193757683205607426', '1195351326706208770', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398730', '1193757683205607426', '1195351566221938690', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398731', '1193757683205607426', '1309071826932019201', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398732', '1193757683205607426', '1195351020463296513', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');
INSERT INTO `acl_role_permission` VALUES ('1309073198490398733', '1193757683205607426', '1309067051809189889', '0', '2020-09-24 18:12:27', '2020-09-24 18:12:27');

-- ----------------------------
-- Table structure for acl_user
-- ----------------------------
DROP TABLE IF EXISTS `acl_user`;
CREATE TABLE `acl_user` (
  `id` char(19) NOT NULL COMMENT '会员id',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '微信openid',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `salt` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `token` varchar(100) DEFAULT NULL COMMENT '用户签名',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of acl_user
-- ----------------------------
INSERT INTO `acl_user` VALUES ('1', 'admin', '96e79218965eb72c92a549dd5a330112', 'admin', '', null, '0', '2019-11-01 10:39:47', '2019-11-01 10:39:47');
INSERT INTO `acl_user` VALUES ('1308389536226623489', 'linjiajian', '00cd56ae177ffac203654b4e04af4e3a', '林家健', null, null, '0', '2020-09-22 20:55:49', '2020-09-24 15:27:02');

-- ----------------------------
-- Table structure for acl_user_role
-- ----------------------------
DROP TABLE IF EXISTS `acl_user_role`;
CREATE TABLE `acl_user_role` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '主键id',
  `role_id` char(19) NOT NULL DEFAULT '0' COMMENT '角色id',
  `user_id` char(19) NOT NULL DEFAULT '0' COMMENT '用户id',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_user_role
-- ----------------------------
INSERT INTO `acl_user_role` VALUES ('1307295952635195394', '1196300996034977794', '2', '0', '2020-09-19 20:30:18', '2020-09-19 20:30:18');
INSERT INTO `acl_user_role` VALUES ('1307298608049057793', '1307298047824261121', '1307298297389543425', '0', '2020-09-19 20:40:51', '2020-09-19 20:40:51');
INSERT INTO `acl_user_role` VALUES ('1307298608049057794', '1193757683205607426', '1307298297389543425', '0', '2020-09-19 20:40:51', '2020-09-19 20:40:51');
INSERT INTO `acl_user_role` VALUES ('1309070706495008770', '1193757683205607426', '1308389536226623489', '0', '2020-09-24 18:02:32', '2020-09-24 18:02:32');

-- ----------------------------
-- Table structure for cms_banner
-- ----------------------------
DROP TABLE IF EXISTS `cms_banner`;
CREATE TABLE `cms_banner` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT 'ID',
  `title` varchar(20) DEFAULT '' COMMENT '标题',
  `image_url` varchar(500) NOT NULL DEFAULT '' COMMENT '图片地址',
  `link_url` varchar(500) DEFAULT '' COMMENT '链接地址',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='首页banner表';

-- ----------------------------
-- Records of cms_banner
-- ----------------------------
INSERT INTO `cms_banner` VALUES ('1194556896025845762', 'banner1', 'https://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/banner/1.jpg', '/course', '1', '0', '2019-11-13 18:05:32', '2020-09-15 17:37:33');
INSERT INTO `cms_banner` VALUES ('1194607458461216769', 'banner2', 'https://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/banner/2.jpg', '/course', '2', '0', '2019-11-13 21:26:27', '2019-11-14 09:12:15');

-- ----------------------------
-- Table structure for edu_chapter
-- ----------------------------
DROP TABLE IF EXISTS `edu_chapter`;
CREATE TABLE `edu_chapter` (
  `id` char(19) NOT NULL COMMENT '章节ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `title` varchar(50) NOT NULL COMMENT '章节名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '显示排序',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

-- ----------------------------
-- Records of edu_chapter
-- ----------------------------
INSERT INTO `edu_chapter` VALUES ('1308376955399299073', '1308376858322132993', '第一章：认识java', '1', '2020-09-22 20:05:49', '2020-09-22 20:05:49');
INSERT INTO `edu_chapter` VALUES ('1308377008859897858', '1308376858322132993', '第二章：Java基础', '2', '2020-09-22 20:06:02', '2020-09-22 20:06:12');
INSERT INTO `edu_chapter` VALUES ('1308377156772028417', '1308376858322132993', '第三章：JVM基础', '3', '2020-09-22 20:06:37', '2020-09-22 20:09:14');
INSERT INTO `edu_chapter` VALUES ('1308377907191734274', '1308376858322132993', '第四章：基础框架', '4', '2020-09-22 20:09:36', '2020-09-22 20:09:36');
INSERT INTO `edu_chapter` VALUES ('1308383647516188674', '1308383621574418434', '第一章', '1', '2020-09-22 20:32:25', '2020-09-22 20:32:25');
INSERT INTO `edu_chapter` VALUES ('1308383664431816706', '1308383621574418434', '第二章', '0', '2020-09-22 20:32:29', '2020-09-22 20:32:29');
INSERT INTO `edu_chapter` VALUES ('1308385243432083458', '1308385207285571586', '第一章', '1', '2020-09-22 20:38:45', '2020-09-22 20:38:45');
INSERT INTO `edu_chapter` VALUES ('1308385258040844290', '1308385207285571586', '第二章', '0', '2020-09-22 20:38:49', '2020-09-22 20:38:49');
INSERT INTO `edu_chapter` VALUES ('1308385272616050690', '1308385207285571586', '第三章', '0', '2020-09-22 20:38:52', '2020-09-22 20:38:52');
INSERT INTO `edu_chapter` VALUES ('1308386094498308098', '1308386046712602626', '第一章', '1', '2020-09-22 20:42:08', '2020-09-22 20:42:08');
INSERT INTO `edu_chapter` VALUES ('1308386116275134465', '1308386046712602626', '第二章', '2', '2020-09-22 20:42:13', '2020-09-22 20:42:13');
INSERT INTO `edu_chapter` VALUES ('1308386140404965377', '1308386046712602626', '第三章', '3', '2020-09-22 20:42:19', '2020-09-22 20:42:19');
INSERT INTO `edu_chapter` VALUES ('1308386748365135874', '1308386726709944321', '第一章', '0', '2020-09-22 20:44:44', '2020-09-22 20:44:44');
INSERT INTO `edu_chapter` VALUES ('1308386769449902081', '1308386726709944321', '第二章', '1', '2020-09-22 20:44:49', '2020-09-22 20:44:49');
INSERT INTO `edu_chapter` VALUES ('1308387410679291905', '1308387387807752193', '第一章', '1', '2020-09-22 20:47:22', '2020-09-22 20:47:22');
INSERT INTO `edu_chapter` VALUES ('1308387431940218881', '1308387387807752193', '第二章', '2', '2020-09-22 20:47:27', '2020-09-22 20:47:27');
INSERT INTO `edu_chapter` VALUES ('1308387453821902850', '1308387387807752193', '第三章', '3', '2020-09-22 20:47:32', '2020-09-22 20:47:32');
INSERT INTO `edu_chapter` VALUES ('1308388451030261762', '1308388429723197441', '第一章', '0', '2020-09-22 20:51:30', '2020-09-22 20:51:30');
INSERT INTO `edu_chapter` VALUES ('1308388469938184193', '1308388429723197441', '第二章', '1', '2020-09-22 20:51:35', '2020-09-22 20:51:35');
INSERT INTO `edu_chapter` VALUES ('1308388493984129025', '1308388429723197441', '第三章', '2', '2020-09-22 20:51:40', '2020-09-22 20:51:40');
INSERT INTO `edu_chapter` VALUES ('1311313201075408898', '1311313159451136001', '第一章', '1', '2020-09-30 22:33:25', '2020-09-30 22:33:25');
INSERT INTO `edu_chapter` VALUES ('1311313223691096065', '1311313159451136001', '第二章', '2', '2020-09-30 22:33:30', '2020-09-30 22:33:30');
INSERT INTO `edu_chapter` VALUES ('1311313256062734338', '1311313159451136001', '第三章', '3', '2020-09-30 22:33:38', '2020-09-30 22:33:38');

-- ----------------------------
-- Table structure for edu_comment
-- ----------------------------
DROP TABLE IF EXISTS `edu_comment`;
CREATE TABLE `edu_comment` (
  `id` char(19) NOT NULL COMMENT '讲师ID',
  `course_id` varchar(19) NOT NULL DEFAULT '' COMMENT '课程id',
  `teacher_id` char(19) NOT NULL DEFAULT '' COMMENT '讲师id',
  `member_id` varchar(19) NOT NULL DEFAULT '' COMMENT '会员id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '会员头像',
  `content` varchar(500) DEFAULT NULL COMMENT '评论内容',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_teacher_id` (`teacher_id`),
  KEY `idx_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='评论';

-- ----------------------------
-- Records of edu_comment
-- ----------------------------

-- ----------------------------
-- Table structure for edu_course
-- ----------------------------
DROP TABLE IF EXISTS `edu_course`;
CREATE TABLE `edu_course` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `teacher_id` char(19) NOT NULL COMMENT '课程讲师ID',
  `subject_id` char(19) NOT NULL COMMENT '课程专业ID',
  `subject_parent_id` char(19) DEFAULT NULL COMMENT '课程专业父级ID',
  `title` varchar(50) NOT NULL COMMENT '课程标题',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '课程销售价格，设置为0则可免费观看',
  `lesson_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总课时',
  `cover` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '课程封面图片路径',
  `buy_count` bigint(10) unsigned NOT NULL DEFAULT '0' COMMENT '销售数量',
  `view_count` bigint(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览数量',
  `version` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `status` varchar(10) NOT NULL DEFAULT 'Draft' COMMENT '课程状态 Draft未发布  Normal已发布',
  `is_deleted` tinyint(3) DEFAULT NULL COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_title` (`title`),
  KEY `idx_subject_id` (`subject_id`),
  KEY `idx_teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

-- ----------------------------
-- Records of edu_course
-- ----------------------------
INSERT INTO `edu_course` VALUES ('1308376858322132993', '1', '1304386984824913921', '1304386984795553793', 'Java快速入门', '0.00', '5', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/764f955fdc354b4aa33666c6967777951442295581911.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:05:26', '2020-09-30 22:22:24');
INSERT INTO `edu_course` VALUES ('1308383621574418434', '1189389726308478977', '1304386984824913921', '1304386984795553793', 'JavaWeb开发', '0.10', '4', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1a3a50ecc3a446dfad4afc98b2192784u=2704972155,1466701427&fm=15&gp=0.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:32:19', '2020-09-30 22:24:29');
INSERT INTO `edu_course` VALUES ('1308385207285571586', '1189390295668469762', '1304386984824913921', '1304386984795553793', 'Java高级', '0.10', '6', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/d2b465d07c864d898d615c9ec8f8c6ccu=356856327,43949763&fm=15&gp=0.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:38:37', '2020-09-30 22:25:13');
INSERT INTO `edu_course` VALUES ('1308386046712602626', '1189426437876985857', '1304386984703279105', '1304386984518729730', 'JavaScript基础与深入解析', '0.00', '3', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/c05add1f5a3d4fd283b16262e120121eu=2729708130,4284426376&fm=26&gp=0.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:41:57', '2020-09-30 22:26:29');
INSERT INTO `edu_course` VALUES ('1308386726709944321', '1304382103250583554', '1304386984556478466', '1304386984518729730', 'Vue从入门到实战', '0.10', '3', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/dd77245afe2e42daaefd1f14e41b3ff5下载.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:44:39', '2020-09-30 22:27:43');
INSERT INTO `edu_course` VALUES ('1308387387807752193', '1308370424041693186', '1304386984766193665', '1304386984518729730', 'jQuery应用与实战开发', '0.01', '3', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/0e8dae076a6546c5920c8786ee7d5ce9下载 (1).jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:47:17', '2020-09-30 22:28:24');
INSERT INTO `edu_course` VALUES ('1308388429723197441', '1189426464967995393', '1304386985017851906', '1304386984984297474', 'MySQL实战', '0.01', '3', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295570359.jpg', '0', '0', '1', 'Normal', null, '2020-09-22 20:51:25', '2020-09-30 22:28:40');
INSERT INTO `edu_course` VALUES ('1311313159451136001', '1304382103250583554', '1304386984933965826', '1304386984795553793', 'Python零基础入门', '0.01', '6', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295455437.jpg', '0', '0', '1', 'Normal', null, '2020-09-30 22:33:15', '2020-09-30 22:34:58');

-- ----------------------------
-- Table structure for edu_course_collect
-- ----------------------------
DROP TABLE IF EXISTS `edu_course_collect`;
CREATE TABLE `edu_course_collect` (
  `id` char(19) NOT NULL COMMENT '收藏ID',
  `course_id` char(19) NOT NULL COMMENT '课程讲师ID',
  `member_id` char(19) NOT NULL DEFAULT '' COMMENT '课程专业ID',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程收藏';

-- ----------------------------
-- Records of edu_course_collect
-- ----------------------------

-- ----------------------------
-- Table structure for edu_course_description
-- ----------------------------
DROP TABLE IF EXISTS `edu_course_description`;
CREATE TABLE `edu_course_description` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `description` text COMMENT '课程简介',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='课程简介';

-- ----------------------------
-- Records of edu_course_description
-- ----------------------------
INSERT INTO `edu_course_description` VALUES ('1104870479077879809', '<p>11</p>', '2019-03-11 06:23:44', '2019-03-11 06:23:44');
INSERT INTO `edu_course_description` VALUES ('1308376858322132993', '<p>本阶段是进入&ldquo;程序员&rdquo;的<strong><span style=\"color: #ff0000;\">门槛</span></strong>，需要学习编程最基本的知识：变量、数据类型、控制语句、面向对象。</p>\n<p>我们通过实际的案例，让大家一开始就通过游戏项目进入学习状态，寓教于乐，引起大家的兴趣。</p>\n<p><strong>&ldquo;兴趣永远是最好的老师，老师只能排第二&rdquo;。</strong></p>\n<p>同时，我们也深入内存底层，打扎实大家的基本功。<img src=\"static/tinymce4.7.5/plugins/emoticons/img/smiley-cool.gif\" alt=\"cool\" /></p>\n<p>本阶段内容学完，你就明白各个语言底层其实差异都不大。大家再去学其他编程语言也很容易入门。</p>', '2020-09-22 20:05:26', '2020-09-30 22:22:18');
INSERT INTO `edu_course_description` VALUES ('1308383621574418434', '<p>本阶段是进入真正企业级项目的起点，是重点也是难点。</p>\n<p>我们学习JAVAEE的核心内容：Servlet和JSP、Tomcat服务器等。</p>\n<p>这些技术在企业中直接应用的概率不高，但是都是核心基础。</p>\n<p>掌握这些内容，再往后学习将非常轻松。<img src=\"static/tinymce4.7.5/plugins/emoticons/img/smiley-embarassed.gif\" alt=\"embarassed\" /></p>', '2020-09-22 20:32:19', '2020-09-30 22:24:25');
INSERT INTO `edu_course_description` VALUES ('1308385207285571586', '<p>本阶段我们更加深入的介绍面对象机制，深入底层和数据结构、再剖析JDK源码底层，这些都是成为JAVA高手必经的磨练。</p>\n<p>同时，增加&ldquo;并发编程&rdquo;课程，高并发问题是大型互联网企业面对的核心问题之一，我们从一开始就让大家理解&ldquo;高并发处理的核心思想&rdquo;。</p>\n<p>最后，我们以一个&ldquo;手写服务器项目&rdquo;收尾。可以说，能完成&ldquo;手写服务器项目&rdquo;基本就迈入了<em><strong><span style=\"color: #ff00ff;\">&ldquo;JAVA小高手&rdquo;</span></strong></em>的行列。</p>', '2020-09-22 20:38:37', '2020-09-30 22:25:13');
INSERT INTO `edu_course_description` VALUES ('1308386046712602626', '<p>JavaScript为前端的描述语言，也是核心。</p>\n<p>在本阶段中，我们主要学习基础JavaScript语法与深入解析JavaScript。</p>\n<p>包含DOM操作同时也涵盖了面向对象和设计模式。希望大家在本阶段可以熟练掌握这些知识点。<img src=\"static/tinymce4.7.5/plugins/emoticons/img/smiley-cool.gif\" alt=\"cool\" /></p>', '2020-09-22 20:41:57', '2020-09-30 22:26:29');
INSERT INTO `edu_course_description` VALUES ('1308386726709944321', '<p><strong>Vue是一套用于构建用户界面的渐进式框架。</strong></p>\n<p>与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。</p>\n<p>Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</p>\n<p>另一方面，当与现代化的工具链以及各种支持类库结合使用时，Vue 也完全能够为复杂的单页应用提供驱动。</p>\n<p>在课程中，我们将详细讲解如何使用vuejs，从基础到项目实战。让你体验开发的愉悦.</p>', '2020-09-22 20:44:39', '2020-09-30 22:27:43');
INSERT INTO `edu_course_description` VALUES ('1308387387807752193', '<p>jQuery是一个快速、简洁的JavaScript框架，jQuery设计的宗旨是<span style=\"color: #008000;\"><em><strong>&ldquo;write Less，Do More&rdquo;</strong></em></span>，即倡导写更少的代码，做更多的事情。</p>\n<p>它封装JavaScript常用的功能代码，提供一种简便的JavaScript设计模式，优化HTML文档操作、事件处理、动画设计和Ajax交互。</p>\n<p>在本阶段，我们注重讲解如何更好的应用jQuery以及他的设计方式</p>', '2020-09-22 20:47:17', '2020-09-30 22:28:24');
INSERT INTO `edu_course_description` VALUES ('1308388429723197441', '<p>任何企业级项目都离不开数据库，数据库知识是程序员的必备技能。</p>\n<p>本阶段我们重点掌握<strong>数据库设计思想</strong>、<strong>SQL语言</strong>。</p>\n<p>同时，我们将JAVA如何操作数据库的技术也引入，讲解了JDBC和Mybatis框架。</p>\n<p>&nbsp;</p>', '2020-09-22 20:51:25', '2020-09-30 22:28:40');
INSERT INTO `edu_course_description` VALUES ('1311313159451136001', '<p><strong>Python</strong> 是一种<strong><span style=\"color: #ff0000;\">解释型、面向对象、动态数据类型的高级程序设计语言</span></strong>。</p>\n<p>本教程适合想从零开始学习 <strong>Python</strong> 编程语言的开发人员。</p>\n<p>当然本教程也会对一些模块进行深入，让你更好的了解<strong> Python</strong> 的<strong><span style=\"color: #ff0000;\">应用</span></strong>。</p>\n<p><strong>本教程主要针对 Python 2.x 版本的学习。</strong></p>', '2020-09-30 22:33:15', '2020-09-30 22:33:15');

-- ----------------------------
-- Table structure for edu_subject
-- ----------------------------
DROP TABLE IF EXISTS `edu_subject`;
CREATE TABLE `edu_subject` (
  `id` char(19) NOT NULL COMMENT '课程类别ID',
  `title` varchar(10) NOT NULL COMMENT '类别名称',
  `parent_id` char(19) NOT NULL DEFAULT '0' COMMENT '父ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程科目';

-- ----------------------------
-- Records of edu_subject
-- ----------------------------
INSERT INTO `edu_subject` VALUES ('1304386984518729730', '前端开发', '0', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984556478466', 'vue', '1304386984518729730', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984703279105', 'JavaScript', '1304386984518729730', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984766193665', 'jquery', '1304386984518729730', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984795553793', '后端开发', '0', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984824913921', 'Java', '1304386984795553793', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984933965826', 'python', '1304386984795553793', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386984984297474', '数据库开发', '0', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');
INSERT INTO `edu_subject` VALUES ('1304386985017851906', 'mysql', '1304386984984297474', '0', '2020-09-11 19:51:06', '2020-09-11 19:51:06');

-- ----------------------------
-- Table structure for edu_teacher
-- ----------------------------
DROP TABLE IF EXISTS `edu_teacher`;
CREATE TABLE `edu_teacher` (
  `id` char(19) NOT NULL COMMENT '讲师ID',
  `name` varchar(20) NOT NULL COMMENT '讲师姓名',
  `intro` varchar(500) NOT NULL DEFAULT '' COMMENT '讲师简介',
  `career` varchar(500) DEFAULT NULL COMMENT '讲师资历,一句话说明讲师',
  `level` int(10) unsigned NOT NULL COMMENT '头衔 1高级讲师 2首席讲师',
  `avatar` varchar(255) DEFAULT NULL COMMENT '讲师头像',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='讲师';

-- ----------------------------
-- Records of edu_teacher
-- ----------------------------
INSERT INTO `edu_teacher` VALUES ('1', '张凯歌', '近年主持国家自然科学基金（6项）、江苏省重大科技成果转化项目（5项）、江苏省产学研前瞻性联合研究项目（3项）、省工业科技支撑、省高技术、省自然科学基金等省部级及其企业的主要科研项目40多个，多个项目在企业成功转化，产生了较好的经济、社会和环境效益。积极开展产学研科技合作，并与省内16家企业建立了江苏省研究生工作站，其中6家为江苏省优秀研究生工作站', '高级讲师', '1', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/571614bc3e714a7cbe31e94cdf83a661file.png', '1', '0', '2019-10-30 14:18:46', '2020-09-30 22:05:49');
INSERT INTO `edu_teacher` VALUES ('1189389726308478977', '袁华', '曾工作广播电视大学、网络营销学院、昂云科技等公司，从事授课工作，教授关于计算机课程的本科学生和专科学生以及中专学生，发起网络营销学院项目，带领团队研发网络营销课程体系，创造年营业额千万。', '首席讲师', '2', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/fdc04110313f4516a04840f5a0e88de0file.png', '2', '0', '2019-10-30 11:53:03', '2020-09-30 22:06:04');
INSERT INTO `edu_teacher` VALUES ('1189390295668469762', '李雯', '浪潮集团前总监级项目经理。精通Java与.NET 技术， 熟练的跨平台面向对象开发经验，技术功底深厚', '首席讲师', '2', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/6e079a1d393e46a099b888b6bddb2d24file.png', '3', '0', '2019-10-30 11:55:19', '2020-09-30 22:06:21');
INSERT INTO `edu_teacher` VALUES ('1189426437876985857', '王二', '高级讲师简介', '高级讲师', '1', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/632b2841c52a43769399eb978884538efile.png', '0', '0', '2019-10-30 14:18:56', '2020-09-30 22:06:39');
INSERT INTO `edu_teacher` VALUES ('1189426464967995393', '吴翠花', '浪潮集团前总监级项目经理。精通Java与.NET 技术， 熟练的跨平台面向对象开发经验，技术功底深厚', '高级讲师', '1', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/a2ce2e52f5a2490085a554bf37585bf8file.png', '0', '0', '2019-10-30 14:19:02', '2020-09-30 22:07:02');
INSERT INTO `edu_teacher` VALUES ('1192249914833055746', '林燕', '曾工作广播电视大学、网络营销学院、昂云科技等公司，从事授课工作，教授关于计算机课程的本科学生和专科学生以及中专学生，发起网络营销学院项目，带领团队研发网络营销课程体系，创造年营业额千万。', '高级讲师', '1', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/f3de8d6dd140447c94044e1548d54c2bfile.png', '0', '0', '2019-11-07 09:18:25', '2020-09-30 22:07:24');
INSERT INTO `edu_teacher` VALUES ('1304382103250583554', '李健', '具有十年软件开发经验，曾就职于中海技创公司，历任高级软件开发工程师，项目经理', '首席讲师', '2', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/e5004b0a77654d9f93274c6cb35be37afile.png', '4', '0', '2020-09-11 19:31:42', '2020-09-30 22:07:49');
INSERT INTO `edu_teacher` VALUES ('1308370424041693186', '刘继文', '具有二十年的软件开发经验，曾就职于大唐，人民银行，担任高级软件开发工程师，研发项目经理等职务', '首席讲师', '2', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/94c5dd579473495aad7e9bc24cdb5849file.png', '5', '0', '2020-09-22 19:39:52', '2020-09-30 22:08:02');
INSERT INTO `edu_teacher` VALUES ('1308372883157934081', '测试', '手打', '手打', '1', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8ccc7732cff94a0ba6f7e808e6754917file.png', '2', '1', '2020-09-22 19:49:38', '2020-09-22 19:49:38');

-- ----------------------------
-- Table structure for edu_video
-- ----------------------------
DROP TABLE IF EXISTS `edu_video`;
CREATE TABLE `edu_video` (
  `id` char(19) NOT NULL COMMENT '视频ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `chapter_id` char(19) NOT NULL COMMENT '章节ID',
  `title` varchar(50) NOT NULL COMMENT '节点名称',
  `video_source_id` varchar(100) DEFAULT NULL COMMENT '云端视频资源',
  `video_original_name` varchar(100) DEFAULT NULL COMMENT '原始文件名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `play_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '播放次数',
  `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可以试听：0收费 1免费',
  `duration` float NOT NULL DEFAULT '0' COMMENT '视频时长（秒）',
  `status` varchar(20) NOT NULL DEFAULT 'Empty' COMMENT 'Empty未上传 Transcoding转码中  Normal正常',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '视频源文件大小（字节）',
  `version` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_chapter_id` (`chapter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程视频';

-- ----------------------------
-- Records of edu_video
-- ----------------------------
INSERT INTO `edu_video` VALUES ('1308381338077880321', '1308376858322132993', '1308376955399299073', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:23:14', '2020-09-22 20:23:14');
INSERT INTO `edu_video` VALUES ('1308381488116523010', '1308376858322132993', '1308377008859897858', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:23:50', '2020-09-22 20:23:50');
INSERT INTO `edu_video` VALUES ('1308382722391138305', '1308376858322132993', '1308377156772028417', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:28:44', '2020-09-22 20:28:44');
INSERT INTO `edu_video` VALUES ('1308382756134313986', '1308376858322132993', '1308377907191734274', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:28:52', '2020-09-22 20:28:52');
INSERT INTO `edu_video` VALUES ('1308382789323841537', '1308376858322132993', '1308377907191734274', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:29:00', '2020-09-22 20:29:00');
INSERT INTO `edu_video` VALUES ('1308383713152851970', '1308383621574418434', '1308383647516188674', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:32:40', '2020-09-22 20:32:40');
INSERT INTO `edu_video` VALUES ('1308383763710992386', '1308383621574418434', '1308383647516188674', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:32:53', '2020-09-22 20:32:53');
INSERT INTO `edu_video` VALUES ('1308383797680660482', '1308383621574418434', '1308383664431816706', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:33:01', '2020-09-22 20:33:01');
INSERT INTO `edu_video` VALUES ('1308383824641646593', '1308383621574418434', '1308383664431816706', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:33:07', '2020-09-22 20:33:07');
INSERT INTO `edu_video` VALUES ('1308385290240516098', '1308385207285571586', '1308385243432083458', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:38:56', '2020-09-22 20:38:56');
INSERT INTO `edu_video` VALUES ('1308385316014514177', '1308385207285571586', '1308385258040844290', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:39:03', '2020-09-22 20:39:03');
INSERT INTO `edu_video` VALUES ('1308385334603669506', '1308385207285571586', '1308385272616050690', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:39:07', '2020-09-22 20:39:07');
INSERT INTO `edu_video` VALUES ('1308385412525449217', '1308385207285571586', '1308385258040844290', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:39:26', '2020-09-22 20:39:26');
INSERT INTO `edu_video` VALUES ('1308385439595487233', '1308385207285571586', '1308385272616050690', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:39:32', '2020-09-22 20:39:32');
INSERT INTO `edu_video` VALUES ('1308385466367729666', '1308385207285571586', '1308385272616050690', '课时3', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:39:38', '2020-09-22 20:39:38');
INSERT INTO `edu_video` VALUES ('1308386174307524609', '1308386046712602626', '1308386094498308098', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:42:27', '2020-09-22 20:42:27');
INSERT INTO `edu_video` VALUES ('1308386197028069378', '1308386046712602626', '1308386116275134465', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:42:33', '2020-09-22 20:42:33');
INSERT INTO `edu_video` VALUES ('1308386227046703106', '1308386046712602626', '1308386140404965377', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:42:40', '2020-09-22 20:42:40');
INSERT INTO `edu_video` VALUES ('1308386795748188161', '1308386726709944321', '1308386748365135874', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:44:55', '2020-09-22 20:44:55');
INSERT INTO `edu_video` VALUES ('1308386823535452161', '1308386726709944321', '1308386748365135874', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:45:02', '2020-09-22 20:45:02');
INSERT INTO `edu_video` VALUES ('1308386846914502658', '1308386726709944321', '1308386769449902081', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:45:08', '2020-09-22 20:45:08');
INSERT INTO `edu_video` VALUES ('1308387499074248705', '1308387387807752193', '1308387410679291905', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:47:43', '2020-09-22 20:47:43');
INSERT INTO `edu_video` VALUES ('1308387512512798722', '1308387387807752193', '1308387431940218881', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:47:46', '2020-09-22 20:47:46');
INSERT INTO `edu_video` VALUES ('1308387524244267009', '1308387387807752193', '1308387453821902850', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:47:49', '2020-09-22 20:47:49');
INSERT INTO `edu_video` VALUES ('1308388508525780993', '1308388429723197441', '1308388451030261762', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:51:44', '2020-09-22 20:51:44');
INSERT INTO `edu_video` VALUES ('1308388524640296961', '1308388429723197441', '1308388469938184193', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:51:48', '2020-09-22 20:51:48');
INSERT INTO `edu_video` VALUES ('1308388542709358594', '1308388429723197441', '1308388493984129025', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2020-09-22 20:51:52', '2020-09-22 20:51:52');
INSERT INTO `edu_video` VALUES ('1311313287163498497', '1311313159451136001', '1311313201075408898', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:33:45', '2020-09-30 22:33:45');
INSERT INTO `edu_video` VALUES ('1311313396504809473', '1311313159451136001', '1311313201075408898', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:34:11', '2020-09-30 22:34:11');
INSERT INTO `edu_video` VALUES ('1311313468936245250', '1311313159451136001', '1311313223691096065', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:34:29', '2020-09-30 22:34:29');
INSERT INTO `edu_video` VALUES ('1311313494550859777', '1311313159451136001', '1311313223691096065', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:34:35', '2020-09-30 22:34:35');
INSERT INTO `edu_video` VALUES ('1311313532228292609', '1311313159451136001', '1311313256062734338', '课时1', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '1', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:34:44', '2020-09-30 22:34:44');
INSERT INTO `edu_video` VALUES ('1311313567192010754', '1311313159451136001', '1311313256062734338', '课时2', '5044df2a06d049b8ad9d2b98eed2a605', 'trailer.mp4', '2', '0', '0', '0', 'Empty', '0', '1', '2020-09-30 22:34:52', '2020-09-30 22:34:52');

-- ----------------------------
-- Table structure for statistics_daily
-- ----------------------------
DROP TABLE IF EXISTS `statistics_daily`;
CREATE TABLE `statistics_daily` (
  `id` char(19) NOT NULL COMMENT '主键',
  `date_calculated` varchar(20) NOT NULL COMMENT '统计日期',
  `register_num` int(11) NOT NULL DEFAULT '0' COMMENT '注册人数',
  `login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录人数',
  `video_view_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日播放视频数',
  `course_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日新增课程数',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `statistics_day` (`date_calculated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站统计日数据';

-- ----------------------------
-- Records of statistics_daily
-- ----------------------------
INSERT INTO `statistics_daily` VALUES ('1306916111922524161', '2019-03-16', '12', '145', '185', '105', '2020-09-18 19:20:57', '2020-09-18 19:20:57');
INSERT INTO `statistics_daily` VALUES ('1306916223788806146', '2019-01-19', '6', '177', '123', '185', '2020-09-18 19:21:24', '2020-09-18 19:21:24');
INSERT INTO `statistics_daily` VALUES ('1306916502265425921', '2019-11-05', '2', '120', '122', '174', '2020-09-18 19:22:30', '2020-09-18 19:22:30');
INSERT INTO `statistics_daily` VALUES ('1306916699359965185', '2020-09-16', '3', '131', '171', '106', '2020-09-18 19:23:17', '2020-09-18 19:23:17');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `course_id` varchar(19) NOT NULL DEFAULT '' COMMENT '课程id',
  `course_title` varchar(100) DEFAULT NULL COMMENT '课程名称',
  `course_cover` varchar(255) DEFAULT NULL COMMENT '课程封面',
  `teacher_name` varchar(20) DEFAULT NULL COMMENT '讲师名称',
  `member_id` varchar(19) NOT NULL DEFAULT '' COMMENT '会员id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `mobile` varchar(11) DEFAULT NULL COMMENT '会员手机',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '订单金额（分）',
  `pay_type` tinyint(3) DEFAULT NULL COMMENT '支付类型（1：微信 2：支付宝）',
  `status` tinyint(3) DEFAULT NULL COMMENT '订单状态（0：未支付 1：已支付）',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_order_no` (`order_no`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单';

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('1308397111798149121', '20200922212554636', '1308387387807752193', 'jQuery应用与实战开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/b6795c88f5884a898923110c645d2723下载.jpg', '刘继文', '1306155982692315138', '林家健', '15907741682', '0.01', '1', '1', '0', '2020-09-22 21:25:55', '2020-09-22 21:26:22');
INSERT INTO `t_order` VALUES ('1308400128647770113', '20200922213754105', '1308387387807752193', 'jQuery应用与实战开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/b6795c88f5884a898923110c645d2723下载.jpg', '刘继文', '1306216341100851202', 'Ljj', '', '0.01', '1', '1', '0', '2020-09-22 21:37:54', '2020-09-22 21:38:06');
INSERT INTO `t_order` VALUES ('1309076781329813505', '20200924182640942', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306216341100851202', 'Ljj', '', '0.10', '1', '0', '0', '2020-09-24 18:26:41', '2020-09-24 18:26:41');
INSERT INTO `t_order` VALUES ('1310558608154554369', '20200928203455528', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:34:56', '2020-09-28 20:34:56');
INSERT INTO `t_order` VALUES ('1310558608154554370', '20200928203455183', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:34:56', '2020-09-28 20:34:56');
INSERT INTO `t_order` VALUES ('1310558608154554371', '20200928203455872', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:34:56', '2020-09-28 20:34:56');
INSERT INTO `t_order` VALUES ('1310558608532041729', '20200928203455178', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:34:56', '2020-09-28 20:34:56');
INSERT INTO `t_order` VALUES ('1310558609601589249', '20200928203456347', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:34:56', '2020-09-28 20:34:56');
INSERT INTO `t_order` VALUES ('1310558760231628801', '20200928203532643', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:35:32', '2020-09-28 20:35:32');
INSERT INTO `t_order` VALUES ('1310558934232330241', '20200928203613876', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:36:14', '2020-09-28 20:36:14');
INSERT INTO `t_order` VALUES ('1310559047101050881', '20200928203640522', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:36:40', '2020-09-28 20:36:40');
INSERT INTO `t_order` VALUES ('1310559064591298562', '20200928203644570', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:36:45', '2020-09-28 20:36:45');
INSERT INTO `t_order` VALUES ('1310559189866770434', '20200928203714750', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:37:15', '2020-09-28 20:37:15');
INSERT INTO `t_order` VALUES ('1310559234099900417', '20200928203725309', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:37:25', '2020-09-28 20:37:25');
INSERT INTO `t_order` VALUES ('1310559418515058689', '20200928203809572', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:38:09', '2020-09-28 20:38:09');
INSERT INTO `t_order` VALUES ('1310559458000236546', '20200928203818552', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:38:18', '2020-09-28 20:38:18');
INSERT INTO `t_order` VALUES ('1310559472076320769', '20200928203821287', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:38:22', '2020-09-28 20:38:22');
INSERT INTO `t_order` VALUES ('1310559495556034561', '20200928203827832', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:38:27', '2020-09-28 20:38:27');
INSERT INTO `t_order` VALUES ('1310559581958696962', '20200928203848544', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:38:48', '2020-09-28 20:38:48');
INSERT INTO `t_order` VALUES ('1310559735256313858', '20200928203924547', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/22/8200e510ae9540a89841189c380a700eu=3961889443,4090747166&fm=26&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-09-28 20:39:25', '2020-09-28 20:39:25');
INSERT INTO `t_order` VALUES ('1311943708825894914', '20201002161849378', '1311313159451136001', 'Python零基础入门', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295455437.jpg', '李健', '1306216341100851202', 'Ljj', '', '0.01', '1', '1', '0', '2020-10-02 16:18:50', '2020-10-02 16:19:34');
INSERT INTO `t_order` VALUES ('1311951511317647362', '20201002164949507', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1a3a50ecc3a446dfad4afc98b2192784u=2704972155,1466701427&fm=15&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-10-02 16:49:50', '2020-10-02 16:49:50');
INSERT INTO `t_order` VALUES ('1311955494727847938', '20201002170539118', '1308388429723197441', 'MySQL实战', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295570359.jpg', '吴翠花', '1306155982692315138', '林家健', '15907741682', '0.01', '1', '0', '0', '2020-10-02 17:05:40', '2020-10-02 17:05:40');
INSERT INTO `t_order` VALUES ('1311971030681522178', '20201002180723067', '1308385207285571586', 'Java高级', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/d2b465d07c864d898d615c9ec8f8c6ccu=356856327,43949763&fm=15&gp=0.jpg', '李雯', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-10-02 18:07:24', '2020-10-02 18:07:24');
INSERT INTO `t_order` VALUES ('1311975125966393346', '20201002182340427', '1311313159451136001', 'Python零基础入门', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295455437.jpg', '李健', '1306155982692315138', '林家健', '15907741682', '0.01', '1', '1', '0', '2020-10-02 18:23:40', '2020-10-02 18:24:00');
INSERT INTO `t_order` VALUES ('1311988110810157058', '20201002191515969', '1308383621574418434', 'JavaWeb开发', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1a3a50ecc3a446dfad4afc98b2192784u=2704972155,1466701427&fm=15&gp=0.jpg', '袁华', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-10-02 19:15:16', '2020-10-02 19:15:16');
INSERT INTO `t_order` VALUES ('1312017290855714817', '20201002211112091', '1308385207285571586', 'Java高级', 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/d2b465d07c864d898d615c9ec8f8c6ccu=356856327,43949763&fm=15&gp=0.jpg', '李雯', '1306155982692315138', '林家健', '15907741682', '0.10', '1', '0', '0', '2020-10-02 21:11:13', '2020-10-02 21:11:13');

-- ----------------------------
-- Table structure for t_pay_log
-- ----------------------------
DROP TABLE IF EXISTS `t_pay_log`;
CREATE TABLE `t_pay_log` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付完成时间',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '支付金额（分）',
  `transaction_id` varchar(30) DEFAULT NULL COMMENT '交易流水号',
  `trade_state` char(20) DEFAULT NULL COMMENT '交易状态',
  `pay_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付类型（1：微信 2：支付宝）',
  `attr` text COMMENT '其他属性',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付日志表';

-- ----------------------------
-- Records of t_pay_log
-- ----------------------------
INSERT INTO `t_pay_log` VALUES ('1308397226814353409', '20200922212554636', '2020-09-22 21:26:22', '0.01', '4200000760202009221297477749', 'SUCCESS', '1', '{\"transaction_id\":\"4200000760202009221297477749\",\"nonce_str\":\"B7E2uU32OmSDLltd\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuGfbNDhAXTk95FBLzTq8IyI\",\"sign\":\"FF3AC69A59B35E1EFEADF859EDECDF2C\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20200922212554636\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20200922212619\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2020-09-22 21:26:22', '2020-09-22 21:26:22');
INSERT INTO `t_pay_log` VALUES ('1308400178555793409', '20200922213754105', '2020-09-22 21:38:06', '0.01', '4200000766202009227124522332', 'SUCCESS', '1', '{\"transaction_id\":\"4200000766202009227124522332\",\"nonce_str\":\"0biz3fBWeIMdd6eD\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuGfbNDhAXTk95FBLzTq8IyI\",\"sign\":\"64DAD994338E5D05429CFDC9C518A4D8\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20200922213754105\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20200922213804\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2020-09-22 21:38:06', '2020-09-22 21:38:06');
INSERT INTO `t_pay_log` VALUES ('1311943893182332930', '20201002161849378', '2020-10-02 16:19:34', '0.01', '4200000772202010027803718050', 'SUCCESS', '1', '{\"transaction_id\":\"4200000772202010027803718050\",\"nonce_str\":\"EWyqwLEnurN8pRqQ\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuGfbNDhAXTk95FBLzTq8IyI\",\"sign\":\"9CC0246DAB726A68D9190890375A12FE\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20201002161849378\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20201002161928\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2020-10-02 16:19:34', '2020-10-02 16:19:34');
INSERT INTO `t_pay_log` VALUES ('1311975209978302465', '20201002182340427', '2020-10-02 18:24:00', '0.01', '4200000759202010025486327670', 'SUCCESS', '1', '{\"transaction_id\":\"4200000759202010025486327670\",\"nonce_str\":\"AnuNPTDBUk1X77oZ\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuGfbNDhAXTk95FBLzTq8IyI\",\"sign\":\"945A2E26B2115765C99FE5F69C95F77D\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20201002182340427\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20201002182352\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2020-10-02 18:24:00', '2020-10-02 18:24:00');

-- ----------------------------
-- Table structure for ucenter_member
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_member`;
CREATE TABLE `ucenter_member` (
  `id` char(19) NOT NULL COMMENT '会员id',
  `openid` varchar(128) DEFAULT NULL COMMENT '微信openid',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(2) unsigned DEFAULT NULL COMMENT '性别 1 女，2 男',
  `age` tinyint(3) unsigned DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `sign` varchar(100) DEFAULT NULL COMMENT '用户签名',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用 1（true）已禁用，  0（false）未禁用',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员表';

-- ----------------------------
-- Records of ucenter_member
-- ----------------------------
INSERT INTO `ucenter_member` VALUES ('1', null, '13700000001', '96e79218965eb72c92a549dd5a330112', '小三123', '1', '5', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-01 12:11:33', '2019-11-08 11:56:01');
INSERT INTO `ucenter_member` VALUES ('1080736474267144193', null, '13700000011', '96e79218965eb72c92a549dd5a330112', '用户XJtDfaYeKk', '1', '19', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-02 12:12:45', '2019-01-02 12:12:56');
INSERT INTO `ucenter_member` VALUES ('1080736474355224577', null, '13700000002', '96e79218965eb72c92a549dd5a330112', '用户wUrNkzAPrc', '1', '27', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-02 12:13:56', '2019-01-02 12:14:07');
INSERT INTO `ucenter_member` VALUES ('1086387099449442306', null, '13520191388', '96e79218965eb72c92a549dd5a330112', '用户XTMUeHDAoj', '2', '20', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1086387099520745473', null, '13520191389', '96e79218965eb72c92a549dd5a330112', '用户vSdKeDlimn', '1', '21', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1086387099608825858', null, '13520191381', '96e79218965eb72c92a549dd5a330112', '用户EoyWUVXQoP', '1', '18', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1086387099701100545', null, '13520191382', '96e79218965eb72c92a549dd5a330112', '用户LcAYbxLNdN', '2', '24', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1086387099776598018', null, '13520191383', '96e79218965eb72c92a549dd5a330112', '用户dZdjcgltnk', '2', '25', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1086387099852095490', null, '13520191384', '96e79218965eb72c92a549dd5a330112', '用户wNHGHlxUwX', '2', '23', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-01-19 06:17:23', '2019-01-19 06:17:23');
INSERT INTO `ucenter_member` VALUES ('1106746895272849410', 'o1R-t5u2TfEVeVjO9CPGdHPNw-to', null, null, '檀梵\'', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/zZfLXcetf2Rpsibq6HbPUWKgWSJHtha9y1XBeaqluPUs6BYicW1FJaVqj7U3ozHd3iaodGKJOvY2PvqYTuCKwpyfQ/132', null, '0', '0', '2019-03-16 10:39:57', '2019-03-16 10:39:57');
INSERT INTO `ucenter_member` VALUES ('1106822699956654081', null, null, null, null, null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-03-16 15:41:10', '2019-03-16 15:41:10');
INSERT INTO `ucenter_member` VALUES ('1106823035660357634', 'o1R-t5i4gENwHYRb5lVFy98Z0bdk', null, null, 'GaoSir', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJI53RcCuc1no02os6ZrattWGiazlPnicoZQ59zkS7phNdLEWUPDk8fzoxibAnXV1Sbx0trqXEsGhXPw/132', null, '0', '0', '2019-03-16 15:42:30', '2019-03-16 15:42:30');
INSERT INTO `ucenter_member` VALUES ('1106823041599492098', null, null, null, null, null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-03-16 15:42:32', '2019-03-16 15:42:32');
INSERT INTO `ucenter_member` VALUES ('1106823115788341250', 'o1R-t5l_3rnbZbn4jWwFdy6Gk6cg', null, null, '换个网名哇、', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/jJHyeM0EN2jhB70LntI3k8fEKe7W6CwykrKMgDJM4VZqCpcxibVibX397p0vmbKURGkLS4jxjGB0GpZfxCicgt07w/132', null, '0', '0', '2019-03-16 15:42:49', '2019-03-16 15:42:49');
INSERT INTO `ucenter_member` VALUES ('1106826046730227714', 'o1R-t5gyxumyBqt0CWcnh0S6Ya1g', null, null, '我是Helen', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDRfib8wy7A2ltERKh4VygxdjVC1x5OaOb1t9hot4JNt5agwaVLdJLcD9vJCNcxkvQnlvLYIPfrZw/132', null, '0', '0', '2019-03-16 15:54:28', '2019-03-16 15:54:28');
INSERT INTO `ucenter_member` VALUES ('1106828185829490690', 'o1R-t5nNlou5lRwBVgGNJFm4rbc4', null, null, ' 虎头', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKxCqRzuYWQmpwiaqQEjNxbC7WicebicXQusU306jgmfoOzUcFg1qaDq5BStiblwBjw5dUOblQ2gUicQOQ/132', null, '0', '0', '2019-03-16 16:02:58', '2019-03-16 16:02:58');
INSERT INTO `ucenter_member` VALUES ('1106830599651442689', 'o1R-t5hZHQB1cbX7HZJsiM727_SA', null, null, '是吴啊', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ9CsqApybcs7f3Dyib9IxIh0sBqJb7LicbjU4WticJFF0PVwFvHgtbFdBwfmk3H2t3NyqmEmVx17tRA/132', null, '0', '0', '2019-03-16 16:12:34', '2019-03-16 16:12:34');
INSERT INTO `ucenter_member` VALUES ('1106830976199278593', 'o1R-t5meKOoyEJ3-IhWRCBKFcvzU', null, null, '我才是Helen', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epMicP9UT6mVjYWdno0OJZkOXiajG0sllJTbGJ9DYiceej2XvbDSGCK8LCF7jv1PuG2uoYlePWic9XO8A/132', null, '0', '0', '2019-03-16 16:14:03', '2019-03-16 16:14:03');
INSERT INTO `ucenter_member` VALUES ('1106831936900415490', 'o1R-t5jXYSWakGtnUBnKbfVT5Iok', null, null, '文若姬', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/3HEmJwpSzguqqAyzmBwqT6aicIanswZibEOicQInQJI3ZY1qmu59icJC6N7SahKqWYv24GvX5KH2fibwt0mPWcTJ3fg/132', null, '0', '0', '2019-03-16 16:17:52', '2019-03-16 16:17:52');
INSERT INTO `ucenter_member` VALUES ('1106832491064442882', 'o1R-t5sud081Qsa2Vb2xSKgGnf_g', null, null, 'Peanut', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-03-16 16:20:04', '2019-03-16 16:20:04');
INSERT INTO `ucenter_member` VALUES ('1106833021442510849', 'o1R-t5lsGc3I8P5bDpHj7m_AIRvQ', null, null, '食物链终结者', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/MQ7qUmCprK9am16M1Ia1Cs3RK0qiarRrl9y8gsssBjIZeS2GwKSrnq7ZYhmrzuzDwBxSMMAofrXeLic9IBlW4M3Q/132', null, '0', '0', '2019-03-16 16:22:11', '2019-03-16 16:22:11');
INSERT INTO `ucenter_member` VALUES ('1191600824445046786', null, '15210078344', '96e79218965eb72c92a549dd5a330112', 'IT妖姬', '1', '5', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-11-05 14:19:10', '2019-11-08 18:04:43');
INSERT INTO `ucenter_member` VALUES ('1191616288114163713', null, '17866603606', '96e79218965eb72c92a549dd5a330112', 'xiaowu', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-11-05 15:20:37', '2019-11-05 15:20:37');
INSERT INTO `ucenter_member` VALUES ('1195187659054329857', null, '15010546384', '96e79218965eb72c92a549dd5a330112', 'qy', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2019-11-15 11:51:58', '2019-11-15 11:51:58');
INSERT INTO `ucenter_member` VALUES ('1306148524179714050', null, 'string', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132', null, '0', '0', '2020-09-16 16:30:50', '2020-09-16 16:30:50');
INSERT INTO `ucenter_member` VALUES ('1306155982692315138', null, '15907741682', 'e10adc3949ba59abbe56e057f20f883e', '林家健', null, null, 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295455437.jpg', null, '0', '0', '2020-09-16 17:00:28', '2020-09-16 17:00:28');
INSERT INTO `ucenter_member` VALUES ('1306216341100851202', 'o3_SC55lEozmrEcdznk5fN_imwFQ', '', null, 'Ljj', null, null, 'http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/2020/09/30/1442295455437.jpg', null, '0', '0', '2020-09-16 21:00:19', '2020-09-16 21:00:19');
