package com.ljj.sms.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.ljj.sms.service.SmsService;
import com.ljj.sms.utils.ConstantPropertiesUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SmsServiceImpl implements SmsService {

    @Override
    public boolean sendVcode(Map<String,Object> param,String phone) throws ClientException {
        if(StringUtils.isEmpty(phone)){
            return false;
        }
        String keyid = ConstantPropertiesUtils.KEY_ID;
        String keysecret  = ConstantPropertiesUtils.KEY_SECRET;
        String signname  = ConstantPropertiesUtils.SIGN_NAME;
        String templatecode  = ConstantPropertiesUtils.TEMPLATE_CODE;

        DefaultProfile profile = DefaultProfile.getProfile("default", keyid, keysecret);
        IAcsClient client = new DefaultAcsClient(profile);

        //设置相关固定的参数
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        //设置发送相关的参数
        request.putQueryParameter("PhoneNumbers",phone); //手机号
        request.putQueryParameter("SignName",signname); //申请阿里云 签名名称
        request.putQueryParameter("TemplateCode",templatecode); //申请阿里云 模板code
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param)); //验证码数据，转换json数据传递


        //发送并获取结果
        CommonResponse response = client.getCommonResponse(request);

        System.out.println(param +" , "+ phone+","+keyid+","+keysecret+","+signname+","+templatecode);

        return response.getHttpResponse().isSuccess();
    }
}
