package com.ljj.sms.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConstantPropertiesUtils implements InitializingBean {

    @Value("${aliyun.sms.keyid}")
    private String keyid;

    @Value("${aliyun.sms.keysecret}")
    private String keysecret;

    @Value("${aliyun.sms.signname}")
    private String signname;

    @Value("${aliyun.sms.templatecode}")
    private String templatecode;



    public static String KEY_ID;

    public static String KEY_SECRET;

    public static String SIGN_NAME;

    public static String TEMPLATE_CODE;


    /**
     * 读取配置文件的值后暴露到公共的static变量供外部访问
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        KEY_ID = keyid;
        KEY_SECRET = keysecret;
        SIGN_NAME = signname;
        TEMPLATE_CODE = templatecode;
    }
}
