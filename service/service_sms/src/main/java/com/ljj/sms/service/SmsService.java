package com.ljj.sms.service;

import com.aliyuncs.exceptions.ClientException;

import java.util.Map;

public interface SmsService {
    boolean sendVcode(Map<String,Object> param,String phone) throws ClientException;
}
