package com.ljj.staservice.controller;


import com.ljj.commonutils.R;
import com.ljj.staservice.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author ljj
 * @since 2020-09-18
 */
@RestController
@RequestMapping("/staservice/statistics")
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService statisticsDailyService;

    @GetMapping("countrRegister/{day}")
    public R countrRegister(@PathVariable String day){
        statisticsDailyService.countRegisterByDay(day);
        return R.ok();
    }

    @GetMapping("{type}/{begin}/{end}")
    public R getStaticticsData(@PathVariable String type,@PathVariable String begin,@PathVariable String end){
        Map<String,Object> map = statisticsDailyService.getStaticticsData(type,begin,end);
        return R.ok().data(map);
    }
}

