package com.ljj.staservice.mapper;

import com.ljj.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author ljj
 * @since 2020-09-18
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
