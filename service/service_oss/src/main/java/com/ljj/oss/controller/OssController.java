package com.ljj.oss.controller;

import com.ljj.oss.service.OssService;
import com.ljj.commonutils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/oss/file")
public class OssController {
    @Autowired
    private OssService ossService;

    @PostMapping
    public R uoloadOssFile(MultipartFile file) throws IOException {
        String url = ossService.uploadAvatar(file);
        return R.ok().data("url",url);
    }
}
