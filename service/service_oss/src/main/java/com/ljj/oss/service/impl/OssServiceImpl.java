package com.ljj.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.ljj.oss.service.OssService;
import com.ljj.oss.utils.ConstantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadAvatar(MultipartFile file) throws IOException {
        //oss地域节点
        String endpoint = ConstantPropertiesUtils.END_POINT;
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = ConstantPropertiesUtils.KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.KEY_SECRET;
        String bucketname = ConstantPropertiesUtils.BUCKET_NAME;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = file.getInputStream();

        //上传到阿里oss服务器
        //参数：bucket名，文件名，文件输入流
        String uuid = java.util.UUID.randomUUID().toString().replaceAll("-","");
        String filename = uuid+file.getOriginalFilename();

        //将文件按照日期进行分类
        String datePath  = new DateTime().toString("yyyy/MM/dd");
        filename = datePath + "/" + filename;

        ossClient.putObject(bucketname, filename, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        //手动拼接url
        //例：http://microcloud-college-oss.oss-cn-shenzhen.aliyuncs.com/01.jpg
        String url = "http://"+bucketname+"."+endpoint+"/"+filename;
        return url;
    }
}
