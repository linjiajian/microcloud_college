package com.ljj.cmsservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.ljj.cmsservice.mapper")
@ComponentScan(basePackages = "com.ljj")
public class CmsBannerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsBannerApplication.class,args);
    }
}
