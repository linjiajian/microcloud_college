package com.ljj.cmsservice.mapper;

import com.ljj.cmsservice.entity.CmsBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author ljj
 * @since 2020-09-15
 */
public interface CmsBannerMapper extends BaseMapper<CmsBanner> {

}
