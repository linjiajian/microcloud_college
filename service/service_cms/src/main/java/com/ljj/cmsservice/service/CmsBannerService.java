package com.ljj.cmsservice.service;

import com.ljj.cmsservice.entity.CmsBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author ljj
 * @since 2020-09-15
 */
public interface CmsBannerService extends IService<CmsBanner> {

    List<CmsBanner> FrontBanners();
}
