package com.ljj.cmsservice.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.cmsservice.entity.CmsBanner;
import com.ljj.cmsservice.service.CmsBannerService;
import com.ljj.commonutils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author ljj
 * @since 2020-09-15
 */
@RestController
@RequestMapping("/cmsservice/banneradmin")
public class CmsBannerAdminController {

    @Autowired
    private CmsBannerService bannerService;

    /**
     * 查询banner列表
     * @return
     */
    @GetMapping
    public R pageList(){
        Page<CmsBanner> page = new Page<>();
        bannerService.page(page,null);
        List<CmsBanner> rows = page.getRecords();
        long total = page.getTotal();
        return R.ok().data("rows",rows).data("total",total);
    }

    @GetMapping("{id}")
    public R getBanner(@PathVariable String id){
        CmsBanner banner = bannerService.getById(id);
        return R.ok().data("banner",banner);
    }

    @PostMapping
    @CacheEvict(value = "front",key = "'banners'")
    public R addBanner(@RequestBody CmsBanner banner){
        return bannerService.save(banner) ? R.ok() : R.error();
    }

    @PutMapping
    @CacheEvict(value = "front",key = "'banners'")
    public R updateBanner(@RequestBody CmsBanner banner){
        return bannerService.updateById(banner) ? R.ok() : R.error();
    }

    @DeleteMapping("{id}")
    @CacheEvict(value = "front",key = "'banners'")
    public R deleteBanner(@PathVariable String id){
        return bannerService.removeById(id) ? R.ok() : R.error();
    }
}

