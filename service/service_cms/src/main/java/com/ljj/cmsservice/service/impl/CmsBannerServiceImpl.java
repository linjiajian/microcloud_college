package com.ljj.cmsservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljj.cmsservice.entity.CmsBanner;
import com.ljj.cmsservice.mapper.CmsBannerMapper;
import com.ljj.cmsservice.service.CmsBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author ljj
 * @since 2020-09-15
 */
@Service
public class CmsBannerServiceImpl extends ServiceImpl<CmsBannerMapper, CmsBanner> implements CmsBannerService {

    @Override
    @Cacheable(value = "front",key = "'banners'")
    public List<CmsBanner> FrontBanners() {
        QueryWrapper<CmsBanner> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 2");
        List<CmsBanner> frontBanners = baseMapper.selectList(wrapper);
        return frontBanners;
    }
}
