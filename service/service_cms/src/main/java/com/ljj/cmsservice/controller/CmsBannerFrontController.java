package com.ljj.cmsservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.cmsservice.entity.CmsBanner;
import com.ljj.cmsservice.service.CmsBannerService;
import com.ljj.commonutils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author ljj
 * @since 2020-09-15
 */
@RestController
@RequestMapping("/cmsservice/bannerfront")
public class CmsBannerFrontController {

    @Autowired
    private CmsBannerService bannerService;

    @GetMapping("banner")
    public R list(){
        List<CmsBanner> bannerList = bannerService.FrontBanners();
        return R.ok().data("bannerList",bannerList);
    }
}

