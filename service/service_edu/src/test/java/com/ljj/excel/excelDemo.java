package com.ljj.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class excelDemo {
    @ExcelProperty(value = "学生编号",index = 0)
    private Integer stuNo;
    @ExcelProperty(value = "学生姓名",index = 1)
    private String stuName;

    public static void main(String[] args) {
        //写操作

        //1.设置文件路径和名称
        String filename = "C:/write.xlsx";

        List<excelDemo> data = new ArrayList<>();
        for(int i = 0; i < 10;i++){
            excelDemo temp = new excelDemo();
            temp.setStuNo(i);
            temp.setStuName("amy"+i);
            data.add(temp);
        }
        //2.使用easyexcel写入数据
        EasyExcel.write(filename,excelDemo.class).sheet("学生列表").doWrite(data);

        //读
        EasyExcel.read(filename,excelDemo.class,new excelListener()).sheet().doRead();
    }
}
