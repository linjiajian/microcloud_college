package com.ljj.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import javax.servlet.ReadListener;
import java.util.Map;

public class excelListener extends AnalysisEventListener<excelDemo> {
    @Override
    public void invoke(excelDemo excelDemo, AnalysisContext analysisContext) {
        System.out.println("****"+excelDemo);
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("***"+headMap);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("over");
    }
}
