package com.ljj.eduservice.controller;


import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduChapter;
import com.ljj.eduservice.entity.chapter.ChapterVo;
import com.ljj.eduservice.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/eduservice/chapter")
public class EduChapterController {

    @Autowired
    private EduChapterService chapterService;

    /**
     *  获取所有章节和小节（一个小节对应一个视频）
     *  树结构
     */

    @GetMapping("tree/{courseId}")
    public R getChapterVideo(@PathVariable String courseId){
        List<ChapterVo> list = chapterService.getChapterVideoByCourseId(courseId);
        return  R.ok().data("list",list);
    }

    /**
     * 根据ID查询章节信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getChapter(@PathVariable String id){
        EduChapter chapter = chapterService.getById(id);
        return R.ok().data("chapter",chapter);
    }

    /**
     * 添加章节
     * @param chapter
     * @return
     */
    @PostMapping
    public R addChapter(@RequestBody EduChapter chapter){
        return chapterService.save(chapter) ? R.ok() : R.error();
    }

    /**
     * 修改章节信息
     * @param chapter
     * @return
     */
    @PutMapping
    public R updateChapter(@RequestBody EduChapter chapter){
        return chapterService.updateById(chapter) ?  R.ok() : R.error();
    }

    /**
     * 根据id删除章节
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public R deleteChapter(@PathVariable String id){
        return  chapterService.deleteChapter(id) ? R.ok() : R.error();
    }


}

