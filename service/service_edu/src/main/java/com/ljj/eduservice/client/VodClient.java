package com.ljj.eduservice.client;

import com.ljj.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@FeignClient(name="service-vod",fallback = VodClientFallBack.class)
@Component
public interface VodClient {
    @DeleteMapping(value = "/vod/video/{id}")
    R deleteVideo(@PathVariable("id") String id);
    @DeleteMapping("/vod/video")
    R deleteVideos(@RequestParam("ids") List<String> ids);
}
