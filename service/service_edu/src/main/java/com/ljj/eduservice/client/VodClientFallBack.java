package com.ljj.eduservice.client;

import com.ljj.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VodClientFallBack implements VodClient{
    @Override
    public R deleteVideo(String id) {
        return R.error().message("删除单个视频失败,原因:服务不可用");
    }

    @Override
    public R deleteVideos(List<String> ids) {
        return R.error().message("删除多个视频失败,原因:服务不可用");
    }
}
