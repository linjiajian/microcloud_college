package com.ljj.eduservice.mapper;

import com.ljj.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
