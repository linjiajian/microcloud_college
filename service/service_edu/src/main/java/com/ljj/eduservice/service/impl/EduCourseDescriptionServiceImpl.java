package com.ljj.eduservice.service.impl;

import com.ljj.eduservice.entity.EduCourseDescription;
import com.ljj.eduservice.mapper.EduCourseDescriptionMapper;
import com.ljj.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
