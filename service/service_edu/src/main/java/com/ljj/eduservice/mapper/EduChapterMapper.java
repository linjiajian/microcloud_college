package com.ljj.eduservice.mapper;

import com.ljj.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
