package com.ljj.eduservice.mapper;

import com.ljj.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
