package com.ljj.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ljj.eduservice.entity.frontvo.CourseFrontVo;
import com.ljj.eduservice.entity.frontvo.CourseWebVo;
import com.ljj.eduservice.entity.vo.CourseInfoVo;
import com.ljj.eduservice.entity.vo.CoursePublishVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourse(CourseInfoVo courseInfoVo);
    CourseInfoVo getCourseInfo(String courseId);
    void updateCourseInfo(CourseInfoVo courseInfoVo);
    CoursePublishVo getPublishCourseInfo(String id);
    boolean deleteCourseByID(String id);
    List<EduCourse> frontCourses();

    Map<String, Object> getCourseFrontList(Page<EduCourse> pageCourse, CourseFrontVo courseFrontVo);

    CourseWebVo getFrontCourseInfo(String courseId);
}
