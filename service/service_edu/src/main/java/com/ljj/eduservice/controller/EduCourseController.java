package com.ljj.eduservice.controller;


import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduCourse;
import com.ljj.eduservice.entity.vo.CourseInfoVo;
import com.ljj.eduservice.entity.vo.CoursePublishVo;
import com.ljj.eduservice.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/eduservice/course")
public class EduCourseController {

    @Autowired
    private EduCourseService courseService;

    /**
     * 添加课程信息（课程和简介）
     * @param courseInfoVo
     * @return
     */
    @PostMapping
    public R addCourse(@RequestBody CourseInfoVo courseInfoVo){
        String courseId = courseService.saveCourse(courseInfoVo);
        return R.ok().data("courseId",courseId);
    }

    /**
     * 根据课程Id获取课程信息（课程和简介）
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R getCourseById(@PathVariable String id){
        CourseInfoVo courseInfo = courseService.getCourseInfo(id);
        return R.ok().data("courseInfo",courseInfo);
    }

    /**
     * 修改课程信息（课程和简介）
     * @param courseInfoVo
     * @return
     */
    @PutMapping()
    @CacheEvict(value = "front",key = "'courses'")
    public R updateCourse(@RequestBody CourseInfoVo courseInfoVo){
        courseService.updateCourseInfo(courseInfoVo);
        return R.ok();
    }

    /**
     * 根据课程Id获取最终发布的课程信息
     * @param id
     * @return
     */
    @GetMapping("publish/{id}")
    public R getPublishCourseInfo(@PathVariable String id){
        CoursePublishVo coursePublishVo = courseService.getPublishCourseInfo(id);
        return R.ok().data("coursePublishVo",coursePublishVo);
    }

    /**
     * 根据Id删除课程，包括简介章节小节视频等
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    @CacheEvict(value = "front",key = "'courses'")
    public R deleteCourse(@PathVariable String id){
        return  courseService.deleteCourseByID(id) ? R.ok() : R.error();
    }

    /**
     *发布课程
     * @param id
     * @return
     */
    @PutMapping("{id}")
    @CacheEvict(value = "front",key = "'courses'")
    public R publishCourseByID(@PathVariable String id){
        EduCourse course = new EduCourse();
        course.setId(id);
        course.setStatus("Normal");
        courseService.updateById(course);
        return R.ok();
    }

    /**
     * 获取课程列表
     * @return
     */
    @GetMapping
    public R getCourseList(){
        List<EduCourse> list = courseService.list(null);
        return R.ok().data("list",list);
    }


}

