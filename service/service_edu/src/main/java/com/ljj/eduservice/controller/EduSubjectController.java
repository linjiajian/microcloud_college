package com.ljj.eduservice.controller;


import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduSubject;
import com.ljj.eduservice.entity.subject.OneSubject;
import com.ljj.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
@RestController
@RequestMapping("/eduservice/subject")
public class EduSubjectController {
    @Autowired
    private EduSubjectService subjectService;

    @PostMapping()
    public R addSubject(MultipartFile file) throws IOException {
        subjectService.saveSubject(file);
        return R.ok();
    }

    //课程分类列表（树形）
    @GetMapping
    public R getAllSubject() {
        //list集合泛型是一级分类
        List<OneSubject> list = subjectService.getAllSubject();
        return R.ok().data("list",list);
    }


}

