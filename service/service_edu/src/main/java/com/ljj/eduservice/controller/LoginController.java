package com.ljj.eduservice.controller;

import com.ljj.commonutils.R;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/eduservice/user")
public class LoginController {
    @PostMapping("login")
    public R login(String username, String password){
        return R.ok().data("token","admin");
    }
    @GetMapping("info")
    public R getInfo(String token){
        return R.ok().data("roles","[admin]")
                     .data("name","admin")
                     .data("avatar","https://www.whb.cn/u/cms/www/202009/091606571cmc.jpg");
    }
}
