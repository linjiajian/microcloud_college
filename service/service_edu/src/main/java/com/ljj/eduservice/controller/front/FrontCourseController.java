package com.ljj.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.commonutils.JwtUtils;
import com.ljj.commonutils.R;
import com.ljj.commonutils.ordervo.CourseWebVoOrder;
import com.ljj.eduservice.client.OrderClient;
import com.ljj.eduservice.entity.EduCourse;
import com.ljj.eduservice.entity.chapter.ChapterVo;
import com.ljj.eduservice.entity.frontvo.CourseFrontVo;
import com.ljj.eduservice.entity.frontvo.CourseWebVo;
import com.ljj.eduservice.service.EduChapterService;
import com.ljj.eduservice.service.EduCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/front/course")
public class FrontCourseController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrderClient orderClient;

    //1 条件查询带分页查询课程
    @PostMapping("{page}/{limit}")
    public R getFrontCourseList(@PathVariable long page, @PathVariable long limit,
                                @RequestBody(required = false) CourseFrontVo courseFrontVo) {
        Page<EduCourse> pageCourse = new Page<>(page,limit);
        Map<String,Object> map = courseService.getCourseFrontList(pageCourse,courseFrontVo);
        //返回分页所有数据
        return R.ok().data(map);
    }

    //2 课程详情的方法
    @GetMapping("{courseId}")
    public R getFrontCourseInfo(@PathVariable String courseId, HttpServletRequest request) {
        //根据课程id，编写sql语句查询课程信息
        CourseWebVo courseWebVo = courseService.getFrontCourseInfo(courseId);

        //根据课程id查询章节和小节
        List<ChapterVo> chapterVideoList = chapterService.getChapterVideoByCourseId(courseId);

        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        boolean isBuy = false;
        if(!StringUtils.isEmpty(memberId)){
            isBuy = orderClient.isBuyCourse(courseId,memberId);
        }
        return R.ok().data("courseWebVo",courseWebVo).data("chapterVideoList",chapterVideoList).data("isBuy",isBuy);
    }
    //根据ID获取课程信息，订单模块使用
    @GetMapping("courseorder/{courseId}")
    public CourseWebVoOrder getCourseOrderInfo(@PathVariable String courseId) {
        CourseWebVo courseWebVo = courseService.getFrontCourseInfo(courseId);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();
        BeanUtils.copyProperties(courseWebVo,courseWebVoOrder);
        return courseWebVoOrder;
    }

}












