package com.ljj.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduCourse;
import com.ljj.eduservice.entity.EduTeacher;
import com.ljj.eduservice.service.EduCourseService;
import com.ljj.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/front/teacher")
public class FrontTeacherController {

    @Autowired
    EduTeacherService teacherService;
    @Autowired
    EduCourseService courseService;

    /**
     * 前台讲师列表
     * @param current
     * @param limit
     * @return
     */
    @GetMapping("{current}/{limit}")
    public R getTeacherListPage(@PathVariable Integer current, @PathVariable Integer limit){
        Page<EduTeacher> page = new Page(current,limit);
        Map<String, Object> map = teacherService.getTeacherFrontList(page);
        return R.ok().data(map);
    }

    /**
     * 前台讲师信息及主讲课程
     * @param teacherId
     * @return
     */
    @GetMapping("{teacherId}")
    public R getTeacherListPage(@PathVariable String teacherId){
        EduTeacher teacher = teacherService.getById(teacherId);
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id",teacherId);
        List<EduCourse> courses = courseService.list(wrapper);
        return R.ok().data("teacher",teacher).data("courses",courses);
    }

}
