package com.ljj.eduservice.mapper;

import com.ljj.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljj.eduservice.entity.frontvo.CourseWebVo;
import com.ljj.eduservice.entity.vo.CoursePublishVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {
    CoursePublishVo getPublishCourseInfo(@Param("courseId") String id);
    CourseWebVo getWebCourseInfo(@Param("courseId") String id);
}
