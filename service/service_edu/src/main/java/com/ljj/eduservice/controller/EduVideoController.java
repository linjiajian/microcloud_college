package com.ljj.eduservice.controller;


import com.ljj.commonutils.R;
import com.ljj.eduservice.client.VodClient;
import com.ljj.eduservice.entity.EduVideo;
import com.ljj.eduservice.service.EduVideoService;
import com.ljj.servicebase.handler.MicroCloudException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/eduservice/video")
public class EduVideoController {

    @Autowired
    private EduVideoService videoService;
    @Autowired
    private VodClient vodClient;

    @GetMapping("{id}")
    public R getVideoById(@PathVariable String id){
        EduVideo video = videoService.getById(id);
        return R.ok().data("video",video);
    }

    /**
     * 添加小节
     * @param video
     * @return
     */
    @PostMapping
    public R addVideo(@RequestBody EduVideo video){
        return videoService.save(video) ? R.ok() : R.error();
    }

    /**
     * 修改小节
     * @param video
     * @return
     */
    @PutMapping
    public R updateVideo(@RequestBody EduVideo video){
        return videoService.updateById(video) ? R.ok() : R.error();
    }

    /**
     * 删除小节
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public R deleteVideo(@PathVariable String id){
        videoService.removeVideoById(id);
        return R.ok();
    }
}

