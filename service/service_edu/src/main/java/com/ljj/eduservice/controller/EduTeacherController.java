package com.ljj.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduTeacher;
import com.ljj.eduservice.service.EduTeacherService;
import com.ljj.eduservice.entity.vo.TeacherQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-06
 */
@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
public class EduTeacherController {

    @Autowired
    EduTeacherService eduTeacherService;

    @ApiOperation(value = "查询讲师列表")
    @GetMapping
    public R list(){
        List<EduTeacher> teachers = eduTeacherService.list(null);
        return R.ok().data("items",teachers);
    }

    @ApiOperation(value = "根据讲师ID查询讲师")
    @GetMapping("{id}")
    public R getTeacher(@ApiParam(name = "id",value = "讲师ID") @PathVariable String id){
        EduTeacher teacher = eduTeacherService.getById(id);
        return R.ok().data("teacher",teacher);
    }

    @ApiOperation(value = "分页查询讲师列表")
    @GetMapping("pageList/{current}/{limit}")
    public R pageList(@PathVariable Integer current,@PathVariable Integer limit){
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);
        eduTeacherService.page(pageTeacher,null);
        long total = pageTeacher.getTotal();
        List<EduTeacher> reccords = pageTeacher.getRecords();
        return R.ok().data("total",total).data("rows",reccords);
    }

    @ApiOperation(value = "分页查询讲师列表")
    @PostMapping("pageListCondition/{current}/{limit}")
    public R pageListCondition(@PathVariable Integer current, @PathVariable Integer limit, @RequestBody(required = false) TeacherQuery teacherQuery){
        //分页对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);
        //构建条件
        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        //判断条件值是否为空，不为空拼接条件
        if(!StringUtils.isEmpty(name)){
            queryWrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(level)){
            queryWrapper.eq("level",level);
        }
        if(!StringUtils.isEmpty(begin)){
            queryWrapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)){
            queryWrapper.le("gmt_create",end);
        }
        //分页条件查询
        eduTeacherService.page(pageTeacher,queryWrapper);
        long total = pageTeacher.getTotal();
        List<EduTeacher> reccords = pageTeacher.getRecords();
        return R.ok().data("total",total).data("rows",reccords);
    }

    @ApiOperation(value = "添加讲师")
    @PostMapping
    @CacheEvict(value = "front",key = "'teachers'")
    public R addTeacher(@ApiParam(name = "eduTeacher",value = "讲师对象") @RequestBody EduTeacher eduTeacher){
        return eduTeacherService.save(eduTeacher)? R.ok():R.error();
    }

    @ApiOperation(value = "根据讲师ID修改讲师")
    @PutMapping
    @CacheEvict(value = "front",key = "'teachers'")
    public R updateTeacher(@ApiParam(name = "eduTeacher",value = "讲师对象") @RequestBody EduTeacher eduTeacher){
        return eduTeacherService.updateById(eduTeacher)? R.ok():R.error();
    }

    @ApiOperation(value = "根据Id逻辑删除讲师")
    @DeleteMapping("{id}")
    @CacheEvict(value = "front",key = "'teachers'")
    public R deleteTeacher(@ApiParam(name = "id",value = "讲师Id") @PathVariable String id){
        return eduTeacherService.removeById(id)?R.ok():R.error();
    }
}

