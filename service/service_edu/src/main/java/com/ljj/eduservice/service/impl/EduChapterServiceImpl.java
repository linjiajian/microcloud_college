package com.ljj.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljj.eduservice.entity.EduChapter;
import com.ljj.eduservice.entity.EduVideo;
import com.ljj.eduservice.entity.chapter.ChapterVo;
import com.ljj.eduservice.entity.chapter.VideoVo;
import com.ljj.eduservice.mapper.EduChapterMapper;
import com.ljj.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljj.eduservice.service.EduVideoService;
import com.ljj.servicebase.handler.MicroCloudException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService videoService;

    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {
        //根据课程ID查询所有章节
        EduChapter chapter = new EduChapter();
        QueryWrapper<EduChapter> chapterWrapper = new QueryWrapper();
        chapterWrapper.eq("course_id",courseId);
        List<EduChapter> chapters = baseMapper.selectList(chapterWrapper);

        //根据课程ID查询所有小节（视频/video）
        EduVideo video = new EduVideo();
        QueryWrapper<EduVideo> videoWrapper = new QueryWrapper();
        videoWrapper.eq("course_id",courseId);
        List<EduVideo> videos= videoService.list(videoWrapper);

        //封装章节vo，树结构
        List<ChapterVo> chapterVos = new ArrayList<>();
        for(EduChapter chapterItem : chapters){
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(chapterItem,chapterVo);
            //获取章节的所有小节
            List<VideoVo> videoVos = new ArrayList<>();
            for(EduVideo videoItem : videos){
                if(videoItem.getChapterId().equals(chapterItem.getId())){
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(videoItem,videoVo);
                    videoVos.add(videoVo);
                }
            }
            //将小节封装到章节的children
            chapterVo.setChildren(videoVos);
            chapterVos.add(chapterVo);
        }
        return chapterVos;
    }

    @Override
    public boolean deleteChapter(String id) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",id);
        int count = videoService.count(wrapper);
        if(count > 0){//章节下存在小节，不允许删除
            throw new MicroCloudException(20001,"章节不为空，删除失败");
        }else{
            return baseMapper.deleteById(id) > 0;
        }
    }

    @Override
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper<EduChapter> wrapper = new QueryWrapper();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }
}
