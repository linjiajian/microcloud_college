package com.ljj.eduservice.service;

import com.ljj.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-11
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
