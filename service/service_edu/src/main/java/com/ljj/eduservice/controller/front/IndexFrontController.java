package com.ljj.eduservice.controller.front;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljj.commonutils.R;
import com.ljj.eduservice.entity.EduCourse;
import com.ljj.eduservice.entity.EduTeacher;
import com.ljj.eduservice.entity.vo.TeacherQuery;
import com.ljj.eduservice.service.EduCourseService;
import com.ljj.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-06
 */
@Api(description = "前台首页")
@RestController
@RequestMapping("/eduservice/indexfront")
public class IndexFrontController {

    @Autowired
    EduTeacherService teacherService;
    @Autowired
    EduCourseService courseService;

    @GetMapping("index")
    public R index(){
        List<EduTeacher> teacherList = teacherService.frontTeachers();
        List<EduCourse> courseList = courseService.frontCourses();
        return R.ok().data("teacherList",teacherList).data("courseList",courseList);
    }


}

