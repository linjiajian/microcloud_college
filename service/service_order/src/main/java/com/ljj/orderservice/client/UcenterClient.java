package com.ljj.orderservice.client;

import com.ljj.commonutils.ordervo.UcenterMemberOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-ucenter")
public interface UcenterClient {

    //根据用户id获取用户信息
    @GetMapping("/ucenterservice/member/{id}")
    public UcenterMemberOrder getUserOrderInfo(@PathVariable("id") String id);
}
