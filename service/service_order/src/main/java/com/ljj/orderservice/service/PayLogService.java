package com.ljj.orderservice.service;

import com.ljj.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author ljj
 * @since 2020-09-17
 */
public interface PayLogService extends IService<PayLog> {

    Map createPRCode(String orderNo);

    Map<String,String> queryPayStatus(String orderNo) throws Exception;

    void updateOrderStatus(Map<String,String> map);
}
