package com.ljj.orderservice.client;

import com.ljj.commonutils.ordervo.CourseWebVoOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-edu")
public interface EduClient {

    //根据课程id查询课程信息
    @GetMapping("/eduservice/front/course/courseorder/{id}")
    public CourseWebVoOrder getCourseOrderInfo(@PathVariable("id") String id);

}
