package com.ljj.orderservice.service.impl;

import com.ljj.commonutils.ordervo.CourseWebVoOrder;
import com.ljj.commonutils.ordervo.UcenterMemberOrder;
import com.ljj.orderservice.client.EduClient;
import com.ljj.orderservice.client.UcenterClient;
import com.ljj.orderservice.entity.Order;
import com.ljj.orderservice.mapper.OrderMapper;
import com.ljj.orderservice.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljj.orderservice.utils.OrderNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author ljj
 * @since 2020-09-17
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduClient eduClient;
    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public String createOrder(String courseId, String memberIdByJwtToken) {
        System.out.println(courseId+","+memberIdByJwtToken);
        CourseWebVoOrder courseInfoOrder = eduClient.getCourseOrderInfo(courseId);
        UcenterMemberOrder userInfoOrder = ucenterClient.getUserOrderInfo(memberIdByJwtToken);
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId); //课程id
        order.setCourseTitle(courseInfoOrder.getTitle());
        order.setCourseCover(courseInfoOrder.getCover());
        order.setTeacherName(courseInfoOrder.getTeacherName());
        order.setTotalFee(courseInfoOrder.getPrice());
        order.setMemberId(memberIdByJwtToken);
        order.setMobile(userInfoOrder.getMobile());
        order.setNickname(userInfoOrder.getNickname());
        order.setStatus(0);  //订单状态（0：未支付 1：已支付）
        order.setPayType(1);
        baseMapper.insert(order);
        return order.getOrderNo();
    }
}
