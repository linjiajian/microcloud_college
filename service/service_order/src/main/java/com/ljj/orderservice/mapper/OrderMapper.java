package com.ljj.orderservice.mapper;

import com.ljj.orderservice.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author ljj
 * @since 2020-09-17
 */
public interface OrderMapper extends BaseMapper<Order> {

}
