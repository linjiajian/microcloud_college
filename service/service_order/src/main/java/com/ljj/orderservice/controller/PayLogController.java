package com.ljj.orderservice.controller;


import com.ljj.commonutils.R;
import com.ljj.orderservice.service.PayLogService;
import com.ljj.servicebase.handler.MicroCloudException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author ljj
 * @since 2020-09-17
 */
@RestController
@RequestMapping("/orderservice/pay")
public class PayLogController {

    @Autowired
    private PayLogService payService;

    @GetMapping("QRCode/{orderNo}")
    public R createQRCode(@PathVariable String orderNo){
        Map map = payService.createPRCode(orderNo);
        return R.ok().data(map);
    }
    @GetMapping("status/{orderNo}")
    public R queryPayStatus(@PathVariable String orderNo) throws Exception {
        Map map = payService.queryPayStatus(orderNo);
        if(map==null){
            throw new MicroCloudException(20002,"没有该订单");
        }
        if(!"SUCCESS".equals(map.get("trade_state"))){
            //未支付
            throw new MicroCloudException(20003,"未支付");
        }
        //已支付
        payService.updateOrderStatus(map);
        return R.ok().data(map);
    }
}

