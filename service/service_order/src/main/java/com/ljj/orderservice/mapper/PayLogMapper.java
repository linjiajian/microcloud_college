package com.ljj.orderservice.mapper;

import com.ljj.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author ljj
 * @since 2020-09-17
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
