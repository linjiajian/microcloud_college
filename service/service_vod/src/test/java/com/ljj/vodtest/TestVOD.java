package com.ljj.vodtest;
import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadVideoRequest;
import com.aliyun.vod.upload.resp.UploadVideoResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.*;
import java.util.List;

public class TestVOD {

    public static void upload() {
        String accessKeyId = "LTAI4GKyYzaCeSNpdrcCa9Qy";
        String accessKeySecret = "5mGVlHMAFLsMgktSuHSnCa0Dn5oGJm";

        String title = "6 - What If I Want to Move Faster - upload by sdk";   //上传之后文件名称
        String fileName = "C:/Users/Administrator/Desktop/6 - What If I Want to Move Faster.mp4";  //本地文件路径和名称
        //上传视频的方法
        UploadVideoRequest request = new UploadVideoRequest(accessKeyId, accessKeySecret, title, fileName);
        /* 可指定分片上传时每个分片的大小，默认为2M字节 */
        request.setPartSize(2 * 1024 * 1024L);
        /* 可指定分片上传时的并发线程数，默认为1，(注：该配置会占用服务器CPU资源，需根据服务器情况指定）*/
        request.setTaskNum(1);

        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadVideoResponse response = uploader.uploadVideo(request);

        if (response.isSuccess()) {
            System.out.print("VideoId=" + response.getVideoId() + "\n");
        } else {
            /* 如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因 */
            System.out.print("VideoId=" + response.getVideoId() + "\n");
            System.out.print("ErrorCode=" + response.getCode() + "\n");
            System.out.print("ErrorMessage=" + response.getMessage() + "\n");
        }
    }

    /**
     * 获取播放地址
     * @param
     * @throws ClientException
     */

    public static void getURL() throws ClientException {
        DefaultAcsClient client = InitClient.initVodClient("LTAI4GKyYzaCeSNpdrcCa9Qy","5mGVlHMAFLsMgktSuHSnCa0Dn5oGJm");
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        request.setVideoId("c82e4d7d065d4cad8dc92185e263438d");
        GetPlayInfoResponse response = client.getAcsResponse(request);
        List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
        //播放地址
        for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
            System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
        }
        //Base信息
        System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");
    }

    public static void getAuth(String vid) throws ClientException {
        DefaultAcsClient client = InitClient.initVodClient("LTAI4GKyYzaCeSNpdrcCa9Qy","5mGVlHMAFLsMgktSuHSnCa0Dn5oGJm");
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId(vid);
        GetVideoPlayAuthResponse response = client.getAcsResponse(request);
        System.out.printf("player:"+response.getPlayAuth());
    }

    public static void main(String[] args) throws ClientException {
        getAuth("3101e2498c8547af9c198e56f1f7eb08");
    }

}
