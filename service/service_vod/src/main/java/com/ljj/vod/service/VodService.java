package com.ljj.vod.service;

import com.aliyuncs.exceptions.ClientException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VodService {
    String uploadVideo(MultipartFile file) throws IOException;

    void deleteVideoById(String id) throws ClientException;

    void deleteVideoByIds(List<String> ids) throws ClientException;
}
