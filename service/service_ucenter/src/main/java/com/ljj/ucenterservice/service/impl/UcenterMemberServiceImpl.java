package com.ljj.ucenterservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljj.commonutils.JwtUtils;
import com.ljj.commonutils.MD5;
import com.ljj.servicebase.handler.MicroCloudException;
import com.ljj.ucenterservice.entity.UcenterMember;
import com.ljj.ucenterservice.entity.vo.RegisterVo;
import com.ljj.ucenterservice.mapper.UcenterMemberMapper;
import com.ljj.ucenterservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author ljj
 * @since 2020-09-16
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public String login(UcenterMember member) {
        //非空校验
        String phone = member.getMobile();
        String password = member.getPassword();
        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)){
            throw new MicroCloudException(20001,"手机号或密码为空");
        }

        //查询用户
        QueryWrapper loginWrapper = new QueryWrapper();
        loginWrapper.eq("mobile",member.getMobile());
        UcenterMember dbmember = baseMapper.selectOne(loginWrapper);
        if(dbmember==null){
            throw new MicroCloudException(20001,"手机号未注册");
        }

        //密码校验
        String md5password = MD5.encrypt(member.getPassword());
        if(!md5password.equals(dbmember.getPassword())){
            throw new MicroCloudException(20001,"密码错误");
        }

        //判断用户是否禁用
        if(dbmember.getIsDisabled()) {
            throw new MicroCloudException(20001,"账号被冻结，请联系管理员");
        }

        //生成JWT（JSON WEB TOKEN）
        String token = JwtUtils.getJwtToken(dbmember.getId(),dbmember.getNickname());

        return token;
    }

    @Override
    public void register(RegisterVo registerVo) {
        //参数校验
        String code = registerVo.getCode(); //验证码
        String mobile = registerVo.getMobile(); //手机号
        String nickname = registerVo.getNickname(); //昵称
        String password = registerVo.getPassword(); //密码
        //非空判断
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)
                || StringUtils.isEmpty(code) || StringUtils.isEmpty(nickname)) {
            throw new MicroCloudException(20001,"必填信息不能为空，注册失败");
        }

        //校验手机验证码
        String redisCode = redisTemplate.opsForValue().get(mobile);
        if(StringUtils.isEmpty(redisCode)){
            throw new MicroCloudException(20001,"验证码失效,请重新发送");
        }
        if(!code.equals(redisCode)){
            throw new MicroCloudException(20001,"验证码错误");
        }

        //查询手机号是否被注册
        QueryWrapper registerWrapper = new QueryWrapper();
        registerWrapper.eq("mobile",mobile);
        int count = baseMapper.selectCount(registerWrapper);
        if(count > 0){
            throw new MicroCloudException(20001,"手机号已被注册");
        }

        //保存用户
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));//密码需要加密的
        member.setIsDisabled(false);//用户不禁用
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        baseMapper.insert(member);
    }

    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper =new QueryWrapper<>();
        wrapper.eq("openid",openid);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        return ucenterMember;
    }

    @Override
    public Integer countRegisterByDay(String day) {
        return baseMapper.countRegisterByDay(day);
    }


}
