package com.ljj.ucenterservice.controller;


import com.ljj.commonutils.JwtUtils;
import com.ljj.commonutils.R;
import com.ljj.commonutils.ordervo.UcenterMemberOrder;
import com.ljj.ucenterservice.entity.UcenterMember;
import com.ljj.ucenterservice.entity.vo.RegisterVo;
import com.ljj.ucenterservice.service.UcenterMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author ljj
 * @since 2020-09-16
 */
@RestController
@RequestMapping("/ucenterservice/member")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;

    @PostMapping("login")
    public R userLogin(@RequestBody UcenterMember member){
        String token = memberService.login(member);
        return R.ok().data("token",token);
    }

    @PostMapping("register")
    public R userRigster(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo);
        return R.ok();
    }

    //根据token获取用户信息
    @GetMapping("info")
    public R getMemberInfo(HttpServletRequest request) {
        //调用jwt工具类的方法。根据request对象获取头信息，返回用户id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //查询数据库根据用户id获取用户信息
        UcenterMember member = memberService.getById(memberId);
        return R.ok().data("userInfo",member);
    }

    /**
     * 根据用户ID获取用户信息，并封装成订单用户信息
     */
    @GetMapping("{id}")
    public UcenterMemberOrder getUserOrderInfo(@PathVariable String id){
        UcenterMemberOrder ucenterMemberOrder = new UcenterMemberOrder();
        UcenterMember ucenterMember = memberService.getById(id);
        BeanUtils.copyProperties(ucenterMember,ucenterMemberOrder);
        return ucenterMemberOrder;
    }
    @GetMapping("countRegister/{day}")
    public R countRegisterByDay(@PathVariable String day){
        Integer count = memberService.countRegisterByDay(day);
        return R.ok().data("countRegister",count);
    }
}

