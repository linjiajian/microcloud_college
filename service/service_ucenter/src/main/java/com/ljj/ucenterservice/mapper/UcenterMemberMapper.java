package com.ljj.ucenterservice.mapper;

import com.ljj.ucenterservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author ljj
 * @since 2020-09-16
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {
    public Integer countRegisterByDay(String day);
}
